
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"493",
  
  "macros":[{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=ga.getAll()[0].get(\"clientId\");return a})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=$(\"#wrike-page-tracking-version-control\").text();return a})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var c=[],b=document.querySelectorAll(\".post_tags a\"),a=0;a\u003Cb.length;a+=1)b[a].textContent\u0026\u0026b[a].textContent.length\u0026\u0026c.push(b[a].textContent);if(c.length)return c.join(\" | \")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=document.querySelector(\".post-meta__category-link\").textContent;if(-1\u003Ca.length)return a})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return 1==document.getElementById(\"cEmail\").value?\"corporate\":0==document.getElementById(\"cEmail\").value?\"generic\":\"not_defined\"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){a:{var b=\"uid\\x3d\";for(var d=document.cookie.split(\";\"),a,c=0;c\u003Cd.length;c++){for(a=d[c];\" \"==a.charAt(0);)a=a.substring(1,a.length);if(0===a.indexOf(b)){b=a.substring(b.length,a.length);break a}}b=\"None\"}return b})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return encodeURIComponent(document.location.href)})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){MktCreLevels=[\"C-Level \\x26 Partner\",\"VP\/Head of\",\"Group Manager, Lead, Coordinator\",\"Manager, Sr. Manager, Supervisor, Strategist\",\"Director, Sr. Director, Associate Director\"];itPmLevels=[\"C-Level \\x26 Partner\",\"VP\/Head of\",\"Group Manager, Lead, Coordinator\",\"Director, Sr. Director, Associate Director\"];mapping={Creative:MktCreLevels,Marketing:MktCreLevels,\"IT Ops \\x26 Technology\":itPmLevels,\"Project\/Program management\":itPmLevels};department=document.querySelector(\"[name\\x3dDepartment]\").value;\nlevel=document.querySelector(\"[name\\x3dwrikeBucketLevel]\").value;return icpRoles=mapping[department]\u0026\u0026-1\u003Cmapping[department].indexOf(level)?1:0})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=window.hcTopicName;\"undefined\"==typeof a\u0026\u0026window.url.match(\".*\/posts\/new.*\")\u0026\u0026(a=document.getElementById(\"community_post_topic_id\"),a=a.options[a.selectedIndex].text.split(\" \").join(\"_\"));return a})();"]
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){switch(!0){case \/^(\\\/((\\w{2})|(pt-br)))?\\\/apps(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Apps\";case \/\\\/(mr|cr|pr)\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Behavior-based Homepages\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/blog(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Blog\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/customers(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Customers\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/demo(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Demo\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/free-trial(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Free Plan\";\ncase \/^(\\\/((\\w{2})|(pt-br)))?\\\/gantt-chart(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Gantt Chart\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/google-apps(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Google Apps\";case \/^\\\/hc\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Help Center\";case \/^\\\/(nl|zh-CN|zh-cn|pl)\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Local Onepagers\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/([vw]\\w\\\/)?$\/.test(",["escape",["macro",9],8,16],"):return\"Homepage\";case \/^\\\/login\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Login\";\ncase \/^\\\/main\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Main\";case \/^\\\/project-management-guide(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"PM Guide\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/price(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Price\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/library\\\/.*$\/.test(",["escape",["macro",9],8,16],"):\"Library\";case \/(\\\/((\\w{2})|(pt-br)))?\\\/scheduling-software(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Scheduling software\";case \/^\\\/(joinwrike.htm|cancelled-invitation-external|analyticstracking.htm|declined-invitation|open-wrike-app|remind.htm|reset-password-confirm|frontend\\\/requestforms\\\/index.html|unsubscribe-page|gotoemailcancel.htm|workspace|gws\\\/msteams_login|signupfree|gws\\\/msteams_static_login|accounts.htm|account\\\/customstatus|gws\\\/msteams_getgroupchannel|joinwrikereg.htm)\/.test(",["escape",["macro",9],8,16],"):return\"Service Pages\";\ncase \/^(\\\/((\\w{2})|(pt-br)))?\\\/(marketing-project-management|team-collaboration-software|creative-project-management|project-management|product-management-software|business-operations-teams|professional-services-management|team-collaboration-software)(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Solutions\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/task-management-software(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Task Management\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/timeline-software(-[wv]\\w)?\\\/$\/.test(",["escape",["macro",9],8,16],"):return\"Timeline Software\";\ncase \/^(\\\/((\\w{2})|(pt-br)))?\\\/templates(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Templates\";case \/^(\\\/((\\w{2})|(pt-br)))?\\\/tour(-[wv]\\w)?\\\/.*$\/.test(",["escape",["macro",9],8,16],"):return\"Tour\"}})();"]
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__e"
    },{
      "function":"__u",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__e"
    },{
      "function":"__v",
      "vtp_name":"eventCategory",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventAction",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventLabel",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"eventValue",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"emrr12"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"emrr"
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"country"
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__v",
      "vtp_name":"gtm.scrollThreshold",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.triggers",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":""
    },{
      "function":"__r"
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"adposition",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__f"
    },{
      "function":"__j",
      "vtp_name":"window.wrike.t200"
    },{
      "function":"__aev",
      "vtp_varType":"URL",
      "vtp_component":"URL"
    },{
      "function":"__d",
      "vtp_elementId":"wrike-page-tracking-version-control",
      "vtp_selectorType":"ID"
    },{
      "function":"__k",
      "vtp_name":"_ga"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementId",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",4],
      "vtp_defaultValue":"YvrPCL6995ABEM-j64UD",
      "vtp_map":["list",["map","key","corporate","value","R75aCP_X8JABEM-j64UD"],["map","key","generic","value","YvrPCL6995ABEM-j64UD"]]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"ga_campaign",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_campaign",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_medium",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"collaborate_2019_social"
    },{
      "function":"__k",
      "vtp_name":"_utma"
    },{
      "function":"__k",
      "vtp_name":"ga"
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"utm_source",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"ga_medium",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"ga_source",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"exp"
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"creative"
    },{
      "function":"__smm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",9],
      "vtp_map":["list",["map","key","\/price\/","value","price"],["map","key","\/tour\/","value","tour"],["map","key","\/project-management\/","value","project"],["map","key","\/marketing-project-management\/","value","marketing"]]
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"WrikeBrandedTraffic"
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementId",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__v",
      "vtp_name":"gtm.newUrlFragment",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.oldUrlFragment",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.newHistoryState",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.oldHistoryState",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.historyChangeSource",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoPercent",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.scrollUnits",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.scrollDirection",
      "vtp_dataLayerVersion":1
    }],
  "tags":[{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_doubleClick":true,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","false"],["map","fieldName","cookieDomain","value",".wrike.com"],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_contentGroup":["list",["map","index","1","group",["macro",2]],["map","index","2","group",["macro",3]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-43330364-1",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":1
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":9
    },{
      "function":"__ga",
      "vtp_webPropertyId":"UA-2346671-1",
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_enableInPageLinkId":false,
      "vtp_contentGroup":["list"],
      "vtp_campaignTrack":true,
      "vtp_clientInfo":true,
      "vtp_detectFlash":true,
      "vtp_detectTitle":true,
      "vtp_sendHitsToGoogle":true,
      "vtp_useDebugVersion":false,
      "vtp_forceSsl":false,
      "tag_id":11
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"V_OICPvbsW8Q0_H5ygM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":18
    },{
      "function":"__ua",
      "vtp_trackingId":"UA-43330364-1",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_nonInteraction":false,
      "vtp_contentGroup":["list"],
      "vtp_dimension":["list"],
      "vtp_metric":["list"],
      "vtp_useDebugVersion":false,
      "vtp_enableLinkId":false,
      "vtp_advertisingFeaturesType":"NONE",
      "vtp_eventCategory":["macro",15],
      "vtp_eventAction":["macro",16],
      "vtp_eventLabel":["macro",17],
      "vtp_eventValue":["macro",18],
      "vtp_enableEcommerce":false,
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":23
    },{
      "function":"__lcl",
      "metadata":["map"],
      "vtp_waitForTagsTimeout":"2000",
      "vtp_waitForTags":true,
      "vtp_checkValidation":false,
      "tag_id":25
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"6RItCLTetGcQ0_H5ygM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":49
    },{
      "function":"__sp",
      "once_per_event":true,
      "vtp_customParams":["list",["map","key","google_conversion_format","value","3"]],
      "vtp_conversionId":"1056104813",
      "vtp_customParamsFormat":"USER_SPECIFIED",
      "vtp_enableOgtRmktParams":true,
      "vtp_url":["macro",13],
      "vtp_enableRdpCheckbox":false,
      "tag_id":52
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","cookieDomain","value",".wrike.com"],["map","fieldName","allowLinker","value","false"],["map","fieldName","useAmpClientId","value","true"]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_contentGroup":["list",["map","index","1","group",["macro",2]],["map","index","2","group",["macro",3]],["map","index","3","group",["macro",8]]],
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-43330364-7",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":54
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionId":"935016334",
      "vtp_conversionLabel":"QBwuCL-Jj2gQju_svQM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":55
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":["macro",18],
      "vtp_eventCategory":["macro",15],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",16],
      "vtp_eventLabel":["macro",17],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-43330364-7",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":57
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":61
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"YkJ0CI7xqG0Q0_H5ygM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":65
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"y8PvCIvgh28Q0_H5ygM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":69
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"848267165",
      "vtp_conversionLabel":"_g4UCNr_zXIQnY--lAM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":83
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionValue":["macro",19],
      "vtp_conversionId":"958052401",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"e_o7CKTn3HMQsfDqyAM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":93
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"6RItCLTetGcQ0_H5ygM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":98
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"959886350",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"fz7WCL-L9XUQjujayQM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":100
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"957412038",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"oVmhCLTL5XYQxuXDyAM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":101
    },{
      "function":"__bzi",
      "once_per_event":true,
      "vtp_id":"114863",
      "tag_id":104
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"959730723",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"VEgGCP-funoQo6jRyQM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":105
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"964084416",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"a123CK6tk3wQwIXbywM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":107
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"966806077",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"SAw4CN2zz38QvZSBzQM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":112
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"973915060",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"iZ56CLuP338QtIez0AM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":117
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"978136396",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"s2fWCLaI238QzNq00gM",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":118
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionValue":["macro",20],
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"804998230",
      "vtp_currencyCode":"USD",
      "vtp_conversionLabel":"yZ5RCNmguIIBENaY7f8C",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":123
    },{
      "function":"__cegg",
      "once_per_event":true,
      "vtp_usersNumericId":"00596545",
      "tag_id":124
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"848267165",
      "vtp_conversionLabel":"V3dcCIa_0IcBEJ2PvpQD",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":127
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"ixViCN_7lJMBENPx-coD",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":139
    },{
      "function":"__awct",
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_conversionId":"962492627",
      "vtp_conversionLabel":"8zJkCOiNlZMBENPx-coD",
      "vtp_url":["macro",13],
      "vtp_enableProductReportingCheckbox":true,
      "vtp_enableNewCustomerReportingCheckbox":false,
      "vtp_enableEnhancedConversionsCheckbox":false,
      "vtp_enableRdpCheckbox":false,
      "tag_id":140
    },{
      "function":"__asp",
      "once_per_event":true,
      "vtp_pixelId":"QSZB7NA2DRCDVN2XBCYBLM",
      "vtp_customerId":"52UUGYOERRFQVLGGNUT2ZW",
      "vtp_conversionValueCurrency":"USD",
      "tag_id":142
    },{
      "function":"__flc",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"invmedia",
      "vtp_useImageTag":false,
      "vtp_activityTag":"trial0",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"9554387",
      "vtp_ordinalStandard":["macro",25],
      "vtp_url":["macro",13],
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":157
    },{
      "function":"__flc",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"invmedia",
      "vtp_useImageTag":false,
      "vtp_activityTag":"trial00",
      "vtp_ordinalType":"UNIQUE",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"9554387",
      "vtp_ordinalUnique":"1",
      "vtp_number":["macro",25],
      "vtp_url":["macro",13],
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":161
    },{
      "function":"__flc",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_enableConversionLinker":true,
      "vtp_groupTag":"invmedia",
      "vtp_useImageTag":false,
      "vtp_activityTag":"ebook0",
      "vtp_ordinalType":"STANDARD",
      "vtp_conversionCookiePrefix":"_gcl",
      "vtp_advertiserId":"9554387",
      "vtp_ordinalStandard":["macro",25],
      "vtp_url":["macro",13],
      "vtp_enableGoogleAttributionOptions":false,
      "vtp_showConversionLinkingControls":true,
      "tag_id":162
    },{
      "function":"__cl",
      "tag_id":169
    },{
      "function":"__tl",
      "vtp_eventName":"gtm.timer",
      "vtp_interval":"60000",
      "vtp_limit":"1",
      "vtp_uniqueTriggerId":"215620_346",
      "tag_id":170
    },{
      "function":"__sdl",
      "vtp_verticalThresholdUnits":"PERCENT",
      "vtp_verticalThresholdsPercent":"5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100",
      "vtp_verticalThresholdOn":true,
      "vtp_horizontalThresholdOn":false,
      "vtp_uniqueTriggerId":"215620_347",
      "vtp_enableTriggerStartOption":true,
      "tag_id":171
    },{
      "function":"__hl",
      "tag_id":172
    },{
      "function":"__lcl",
      "vtp_waitForTags":true,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"215620_375",
      "tag_id":173
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function b(){!1===c\u0026\u0026(c=!0,Munchkin.init(\"405-WQD-629\",{cookieAnon:!0}))}var c=!1,a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"\/\/munchkin.marketo.net\/munchkin.js\";a.onreadystatechange=function(){\"complete\"!=this.readyState\u0026\u0026\"loaded\"!=this.readyState||b()};a.onload=b;document.getElementsByTagName(\"head\")[0].appendChild(a)})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":4
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction readCookie(b){b+=\"\\x3d\";for(var d=document.cookie.split(\";\"),c=0;c\u003Cd.length;c++){for(var a=d[c];\" \"==a.charAt(0);)a=a.substring(1,a.length);if(0==a.indexOf(b))return a.substring(b.length,a.length)}return null}function setCookie(b,d,c){getParam(\"gclid\");var a=new Date;a.setTime(a.getTime()+864E5*c);c=\"; expires\\x3d\"+a.toGMTString();null==readCookie(\"gclid\")\u0026\u0026(document.cookie=b+\"\\x3d\"+d+c+\";domain\\x3d.wrike.com;path\\x3d\/\")}\nfunction getParam(b){return(b=RegExp(\"[?\\x26]\"+b+\"\\x3d([^\\x26]*)\").exec(window.location.search))\u0026\u0026decodeURIComponent(b[1].replace(\/\\+\/g,\" \"))}var gclid=getParam(\"gclid\");if(gclid){var gclsrc=getParam(\"gclsrc\");gclsrc\u0026\u0026-1===gclsrc.indexOf(\"aw\")||setCookie(\"gclid\",gclid,90)};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":13
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function l(){var b=h(\"__utmz\");return null!=b?\"\\x26utmz\\x3d\"+b:\"\"}function h(b){b+=\"\\x3d\";for(var f=document.cookie.split(\";\"),g=0;g\u003Cf.length;g++){for(var c=f[g];\" \"==c.charAt(0);)c=c.substring(1,c.length);if(0==c.indexOf(b))return c.substring(b.length,c.length)}return null}var k=200,f=0,n=50,p=setInterval(function(){if(f==n)clearInterval(p);else{var b=h(\"_ga\");if(null==b)f++;else try{var k=document.createElement(\"img\");var g=\"\/analyticstracking.htm\"==document.location.pathname?\"workspace\":\n\"website\";var c=(new Date).getTime().toString()+Math.random().toString().slice(0,4);b=\"\";var a=",["escape",["macro",11],8,16],";if(-1!=a.indexOf(\"\/\")\u0026\u0026(a=a.slice(a.lastIndexOf(\"\/\")+1),a=a.split(\"?\"),1\u003Ca.length)){a=a[1].split(\"\\x26\");for(var e=[],d=0;d\u003Ca.length;d++)-1!=a[d].search(\"utm_\")?e.push(a[d]):-1!=a[d].search(\"ga_\")?e.push(a[d].replace(\"ga_\",\"utm_\")):-1!=a[d].search(\"\\x3d\")\u0026\u0026\"r\"==a[d].split(\"\\x3d\")[0]\u0026\u0026e.push(a[d]);b=\"\\x26\"+e.join(\"\\x26\")}var q=b;console.log(l());var m=document.domain;b=m;a=\"try.wrike.com go.wrike.com help.wrike.com get.wrike.com learn.wrike.com studio.wrike.com cdn.wrike.com library.wrike.com status.wrike.com assets.wrike.com prague.wrike.com\".split(\" \");\nfor(e=0;e\u003Ca.length;e++)if(a[e]==m.toLowerCase()){b=\"www.wrike.com\";break}var r=b;el_src=\"https:\/\/\"+r+\"\/stat\/marketing?area\\x3d\"+g+\"\\x26logic_block\\x3d\"+",["escape",["macro",1],8,16],"+\"\\x26adposition\\x3d\"+",["escape",["macro",26],8,16],"+\"\\x26page\\x3d\"+",["escape",["macro",9],8,16],"+\"\\x26element\\x3dpage\\x26event\\x3dviewed\\x26gaid\\x3d\"+h(\"_ga\")+\"\\x26dc\\x3d\"+c+q+\"\\x26referrer\\x3d\"+encodeURIComponent(",["escape",["macro",27],8,16],")+l()+\"\\x26t200\\x3d\"+",["escape",["macro",28],8,16],";\"undefined\"!=typeof ",["escape",["macro",21],8,16],"\u0026\u0026(el_src+=\"\\x26country\\x3d\"+\n",["escape",["macro",21],8,16],".substring(0,2));k.src=el_src;f=50}catch(t){f=50,console.log(\"tag.manager.tracking:: \"+t.message)}}},k)})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":17
    },{
      "function":"__html",
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){$(\".applist-item\").click(function(){var a=$(this).children(\".applist-item-title\").text();dataLayer.push({event:\"gaEvent\",eventCategory:\"Apps\\x26integrations\",eventAction:a,eventLabel:\"\",eventValue:\"\"})})});\u003C\/script\u003E",
      "vtp_convertJsValuesToExpressions":true,
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":24
    },{
      "function":"__html",
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E$(document).ready(function(){var a=$(\".apps-article-header-name\").text();dataLayer.push({event:\"gaEvent\",eventCategory:\"Clicks_inside_apps_pages\",eventAction:a,eventLabel:",["escape",["macro",29],8,16],"})});\u003C\/script\u003E"],
      "vtp_convertJsValuesToExpressions":true,
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":26
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(e,d){var a=e.location.search.substr(1);if(-1\u003Ca.indexOf(d)){a=a.split(\"\\x26\");for(var c=0;c\u003Ca.length;c++){var b=a[c].split(\"\\x3d\");b[0]===d\u0026\u0026\"undefined\"!==typeof b[1]\u0026\u0026b[1].length\u0026\u0026(console.log(b[1]),dataLayer.push({event:\"gaEvent\",eventCategory:\"test_tracking\",eventAction:b[1],eventLabel:",["escape",["macro",9],8,16],",eventValue:1}))}}})(document,\"trackme\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":32
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_supportDocumentWrite":false,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var b=window._fbq||(window._fbq=[]);if(!b.loaded){var a=document.createElement(\"script\");a.async=!0;a.src=\"\/\/connect.facebook.net\/en_US\/fbds.js\";var c=document.getElementsByTagName(\"script\")[0];c.parentNode.insertBefore(a,c);b.loaded=!0}})();window._fbq=window._fbq||[];window._fbq.push([\"track\",\"6029477390871\",{value:\"0.00\",currency:\"USD\"}]);\u003C\/script\u003E\n\u003Cnoscript\u003E\u003Cimg height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?ev=6029477390871\u0026amp;cd[value]=0.00\u0026amp;cd[currency]=USD\u0026amp;noscript=1\"\u003E\u003C\/noscript\u003E",
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":34
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_supportDocumentWrite":false,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.onload=function(){var a=window.location.pathname.indexOf(\"search\"),b;-1!=a\u0026\u0026(b=decodeURI(window.location.pathname.substring(a+7).replace(RegExp(\"\\\\+\",\"g\"),\" \")));a=document.getElementById(\"SnapABug_Button\");document.getElementsByName(\"s\");null!=a\u0026\u0026a.addEventListener(\"click\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Live_chat\",eventAction:",["escape",["macro",9],8,16],",eventLabel:b})})};\u003C\/script\u003E"],
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":36
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_supportDocumentWrite":false,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.onload=function(){var a=document.getElementById(\"logo_bb\");a\u0026\u0026a.addEventListener(\"click\",function(){dataLayer\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Page\",eventAction:\"Click\",eventLabel:\"Logo\"})})};\u003C\/script\u003E",
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":37
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":" \u003Cscript type=\"text\/gtmscript\"\u003E(function(b,c,e,f,d){b[d]=b[d]||[];var g=function(){var a={ti:\"4056416\"};a.q=b[d];b[d]=new UET(a);b[d].push(\"pageLoad\")};var a=c.createElement(e);a.src=f;a.async=1;a.onload=a.onreadystatechange=function(){var b=this.readyState;b\u0026\u0026\"loaded\"!==b\u0026\u0026\"complete\"!==b||(g(),a.onload=a.onreadystatechange=null)};c=c.getElementsByTagName(e)[0];c.parentNode.insertBefore(a,c)})(window,document,\"script\",\"\/\/bat.bing.com\/bat.js\",\"uetq\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":38
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function f(b){b+=\"\\x3d\";for(var c=document.cookie.split(\";\"),a,d=0;d\u003Cc.length;d++){for(a=c[d];\" \"==a.charAt(0);)a=a.substring(1,a.length);if(0===a.indexOf(b))return a.substring(b.length,a.length)}return null}function k(b,c,a){var d=new Date;d.setTime(d.getTime()+864E5*a);a=d.toUTCString();document.cookie=b+\"\\x3d\"+c+\";expires\\x3d\"+a}var e,l=1E3,m=20,g=0,h=setInterval(function(){g==m\u0026\u0026clearInterval(h);e=f(\"_ga\");null!==e\u0026\u0026f(\"send_dim1\")!=e\u0026\u0026(ga\u0026\u0026ga(function(){if(ga.getAll()){var b=ga.getAll();\nif(b)for(var c=0;c\u003Cb.length;c++)trackerName=b[c].get(\"name\"),clientId=b[c].get(\"clientId\"),\"UA-43330364-1\"==b[c].get(\"trackingId\")\u0026\u0026(ga(trackerName+\".set\",{dimension1:e}),ga(trackerName+\".send\",\"event\",\"get_ga\",\"get_ga1\",\"get_ga1\",{nonInteraction:!0}))}}),k(\"send_dim1\",e,180),clearInterval(h));g+=1},l)})();\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":39
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function c(){setTimeout(function(){d+=1;var a=document.querySelector(\"#SnapABug_WP.online\"),e=document.querySelector(\".modal-support-success\");a||e?dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Submit\",eventLabel:b.join(\" -\\x3e \")}):120\u003Ed\u0026\u0026c()},250)}var b=[],d=0;document.addEventListener(\"click\",function(a){a=a.target;\"#contact\"===a.getAttribute(\"href\")?b[0]=\"Contact support team\":-1!==a.className.indexOf(\"js-btn-open-ticket\")?b[0]=\"Open a ticket\":-1!==a.className.indexOf(\"btn-open-livechat\")\u0026\u0026\n(b[0]=\"Open live chat\");-1!==a.className.indexOf(\"modal-form-support-submit\")?(b[1]=\"Submit a ticket\",c()):\"SnapABug_SB\"===a.id\u0026\u0026(b[1]=\"Start chat\",c())},!0)})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":40
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003E!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version=\"1.1\",a.queue=[],b=e.createElement(f),b.async=!0,b.src=\"\/\/static.ads-twitter.com\/uwt.js\",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,\"script\");twq(\"init\",\"nuvmx\");twq(\"track\",\"PageView\");\u003C\/script\u003E\n\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":44
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){var a=document.getElementById(\"exit-popup-root\");a\u0026\u0026a.addEventListener(\"exitmodal.open\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Pop-up shown\",eventLabel:",["escape",["macro",9],8,16],"+\" (\"+",["escape",["macro",30],8,16],"+\")\"})})});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":46
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){var a=document.getElementById(\"lp-pom-button-1007\"),b=document.getElementById(\"lp-pom-button-1026\");a\u0026\u0026a.addEventListener(\"click\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Click\",eventLabel:",["escape",["macro",9],8,16],"+\" (\"+",["escape",["macro",30],8,16],"+\") \"+this.getAttribute(\"id\")})});b\u0026\u0026b.addEventListener(\"click\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Click\",eventLabel:",["escape",["macro",9],8,16],"+\n\" (\"+",["escape",["macro",30],8,16],"+\") \"+this.getAttribute(\"id\")})})});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":47
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){function a(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Goal\",eventAction:\"Goal_achieved\",eventLabel:\"Time_on_page\"})}setTimeout(a,1E4)});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":48
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction send_event(b,a,c){dataLayer.push({event:\"gaEvent\",eventCategory:b,eventAction:a,eventLabel:c});return!1}function get_domain(b){return-1\u003Cb.indexOf(\":\/\/\")?b.split(\"\/\")[2]:b.split(\"\/\")[0]}\nwindow.addEventListener(\"load\",function(){var b=document.querySelectorAll(\".cat-item\"),a=0;if(b)for(a=0;a\u003Cb.length;a+=1)b[a].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Categories Clicks\",this.textContent)});if(b=document.querySelectorAll(\"#menu-additional-resources .menu-item\"))for(a=0;a\u003Cb.length;a+=1)b[a].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Additional Resources Clicks\",this.textContent)});if(b=document.querySelectorAll(\".rpwe-block .rpwe-li\"))for(a=0;a\u003Cb.length;a+=\n1)b[a].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Individual Blogs Clicks\",this.textContent)});a=document.querySelectorAll(\"#text_icl-6\");a[0]\u0026\u0026a[0].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Graphic Blocks\",\"Download Free PDF\")});a=document.querySelectorAll(\"#text_icl-3\");a[0]\u0026\u0026a[0].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Graphic Blocks\",\"Project Management Guide for Beginners\")});window.$\u0026\u0026$(\"#primary-sidebar\").submit(function(){send_event(\"Sidebar\",\n\"Trial\",\"Start Trial\")});a=document.querySelectorAll(\"#text_icl-5\");a[0]\u0026\u0026a[0].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Graphic Blocks\",\"Learn more\")});b=document.querySelectorAll(\"#wrike_social_links-2 .social_list \\x3e li \\x3e a\");if(b.length)for(a=0;a\u003Cb.length;a+=1)b[a].addEventListener(\"click\",function(){send_event(\"Sidebar\",\"Socials\",get_domain(this.getAttribute(\"href\")))})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":51
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cimg height=\"1\" width=\"1\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?id=307980792700798\u0026amp;ev=PageView\u0026amp;noscript=1\"\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":56
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){window.addEventListener(\"agile.step\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Quiz\",eventLabel:a.detail.step})})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":58
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var h=\"creative\",l={url:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],ga_keyword:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],ga_adgroup:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],ga_campaign:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],utm_term:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],utm_content:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],utm_campaign:[\"marketer\",\"marketing\",\"design\",\"creative\",\"workfront\"],\nutm_adgroup:[\"workfront\"]},m={url:[[\"remarketing\",\"design\"],[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\"creative\"],[\"remarketing\",\"workfront\"]],ga_keyword:[[\"remarketing\",\"design\"],[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\"creative\"],[\"remarketing\",\"workfront\"]],ga_adgroup:[[\"remarketing\",\"design\"],[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\"creative\"],[\"remarketing\",\"workfront\"]],ga_campaign:[[\"remarketing\",\"design\"],\n[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\"creative\"],[\"remarketing\",\"workfront\"]],utm_term:[[\"remarketing\",\"design\"],[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\"creative\"],[\"remarketing\",\"workfront\"]],utm_content:[[\"remarketing\",\"design\"],[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\"creative\"],[\"remarketing\",\"workfront\"]],utm_campaign:[[\"remarketing\",\"design\"],[\"remarketing\",\"marketer\"],[\"remarketing\",\"marketing\"],[\"remarketing\",\n\"creative\"],[\"remarketing\",\"workfront\"]]},n=function(e){var f=\/[?\u0026]([^=#]+)=([^\u0026#]*)\/g,b={},a;for(b.url=e.split(\"?\").shift().toLowerCase();a=f.exec(e);)b[a[1].toLowerCase()]=a[2].toLowerCase();return b},k=function(e,f){var b=new Date;b.setTime(b.getTime()+6048E5);document.cookie=e+\"\\x3d\"+f+\";domain\\x3d.wrike.com;expires\\x3d\"+b.toUTCString()+\";path\\x3d\/\";window.dispatchEvent(new CustomEvent(\"rtp_update\"))},p=function(e){for(var f in e)if(e.hasOwnProperty(f)){var b=e[f],a=l[f],d=m[f];if(\"object\"===\ntypeof a\u0026\u0026a.length){a:{var c=b;for(var g=0;g\u003Ca.length;g++)if(-1!==c.indexOf(a[g])){c=a[g];break a}c=!1}if(!1!==c)if(\"object\"===typeof d\u0026\u0026d.length){a:{a=!1;for(c=0;c\u003Cd.length;c++)if(-1!==b.indexOf(d[c][0])){a=b.replace(d[c][0],\"\");if(-1!==a.indexOf(d[c][1])){d=!0;break a}a=!0}d=!a}if(d)return k(h,1)}else return k(h,1)}}};return-1===document.cookie.indexOf(h+\"\\x3d\")\u0026\u0026p(n(window.location.href))})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":59
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){if(!\/(?:^|; )wrikeVisitor=([^;]+)\/.exec(document.cookie)){if(!\/(?:^|; )firstVisit=([^;]+)\/.exec(document.cookie)){var a=new Date;a.setDate(a.getDate()+365);var b=encodeURIComponent('{\"referrer\":'+(document.referrer?'\"'+document.referrer.substring(0,150)+'\"':null)+',\"uri\":'+(location.pathname?'\"'+location.pathname.substring(0,150)+'\"':null)+',\"query\":'+(location.search?'\"'+location.search.substring(0,150)+'\"':null)+\"}\");b+=\"; expires\\x3d\"+a.toUTCString();b+=\"; domain\\x3dwrike.com\";document.cookie=\n\"firstVisit\\x3d\"+(b+\"; path\\x3d\/\")}if(\"function\"===typeof window.onload)var c=window.onload;window.onload=function(){c\u0026\u0026c();var a=new Image;a.src=(\"https:\"==document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"wrike.com\/statvisitor.gif\";a.style.display=\"none\";document.body.appendChild(a)}}})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":60
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\n\u003Cscript type=\"text\/gtmscript\"\u003Evar _kiq=_kiq||[];(function(){setTimeout(function(){var a=document,b=a.getElementsByTagName(\"script\")[0];a=a.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"https:\/\/cdn.wrike.com\/image\/lp-assets\/scripts\/qualaroo17-2.js\";b.parentNode.insertBefore(a,b)},1)})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":62
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a,b,c){dataLayer.push({event:\"gaEvent\",eventCategory:a,eventAction:b,eventLabel:c});return!1}window.addEventListener(\"gameEvent.loaded\",function(){sendEvent(\"games\",\"loaded\",\"NY game\")});window.addEventListener(\"gameEvent.finished\",function(){sendEvent(\"games\",\"finished\",\"NY game\")});window.addEventListener(\"gameEvent.gameover\",function(){sendEvent(\"games\",\"gameover\",\"NY game\")});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":63
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){(function(){for(var a=document.querySelectorAll(\".new_community_post\"),c=0;c\u003Ca.length;c++)a[c].addEventListener(\"click\",function(b){b=b.target;b.matches(\".searchbox-suggestions a\")\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Community Ticket Deflection\",eventAction:\"Deflect\",eventLabel:b.href})})})();(function(){for(var a=document.querySelectorAll('form[role\\x3d\"search\"]'),c=0;c\u003Ca.length;c++)a[c].addEventListener(\"submit\",function(b){b=b.target;b=b.querySelectorAll('input[type\\x3d\"search\"]');\nb=b.length?b[0].value.toLowerCase():\"\";dataLayer.push({event:\"gaEvent\",eventCategory:\"search\",eventAction:\"search-submit\",eventLabel:b})})})();(function(){for(var a=document.querySelectorAll(\"a.submit-a-request, .article-more-questions a\"),c=0;c\u003Ca.length;c++)a[c].addEventListener(\"click\",function(){var b=window.location.pathname;dataLayer.push({event:\"gaEvent\",eventCategory:\"Submit Request\",eventAction:\"Submit Request Form\",eventLabel:b})})})();(function(){for(var a=document.querySelectorAll(\"#new_request\"),\nc=0;c\u003Ca.length;c++)a[c].addEventListener(\"click\",function(b){b=b.target;b.matches(\".searchbox-suggestions a\")\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Submit Ticket Deflection\",eventAction:\"Deflect\",eventLabel:b.href})})})();(function(){var a=document.getElementById(\"new_request\");a\u0026\u0026a.addEventListener(\"submit\",function(){var a=document.querySelectorAll(\"#new_request .required input, #new_request .required textarea\"),b=0;if(a)for(var d=0;d\u003Ca.length;d++)0\u003Ca[d].value.length?b++:0;a.length\u003C=b\u0026\u0026\ndataLayer.push({event:\"gaEvent\",eventCategory:\"Submit Request form\",eventAction:\"Submit Request Submit button\",eventLabel:location.pathname})})})();(function(){var a=\"\";HelpCenter\u0026\u0026HelpCenter.user\u0026\u0026HelpCenter.user.email\u0026\u0026(a=HelpCenter.user.email);var c=-1!=a.search(\"team.wrike.com\")?\"Internal_user\":\"External_user\";a=document.querySelectorAll(\"form.new_community_post\");var b=document.querySelectorAll(\"form.comment-form\"),d=document.getElementById(\"community_post_title\"),e=document.querySelectorAll(\"form.new_community_post .required a\"),\nf=document.querySelector(\"a.ask-wrike\");a\u0026\u0026a[0]\u0026\u0026a[0].addEventListener(\"submit\",function(){if(d\u0026\u0026e[0]\u0026\u0026\"\"!=d.value\u0026\u0026\"-\"!=e[0].textContent\u0026\u0026\"Internal_user\"!=c){var a=document.getElementById(\"community_post_topic_id\");\"undefined\"!==typeof a\u0026\u0026(hcTopic=a.options[a.selectedIndex].text.split(\" \").join(\"_\"),dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"New_community_post\",eventLabel:hcTopic}))}});b\u0026\u0026b[0]\u0026\u0026b[0].addEventListener(\"submit\",function(){var a=document.getElementById(\"community_comment_body_ifr\");\nif(a)var b=a.contentWindow.document.getElementById(\"tinymce\");b\u0026\u0026\"\"!=b.textContent\u0026\u0026\"Internal_user\"!=c\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"New_comment\",eventLabel:",["escape",["macro",8],8,16],"})});f\u0026\u0026f.addEventListener(\"click\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"Ask Wrike Button Click\",eventLabel:c})})})();(function(){var a=document.querySelectorAll(\".categories .blocks-list .blocks-item\");if(a)for(var c=0;c\u003Ca.length;c++)a[c].addEventListener(\"click\",\nfunction(a){a=a.currentTarget;a.getElementsByTagName(\"h4\")[0]\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"Icons Click\",eventLabel:a.getElementsByTagName(\"h4\")[0].textContent})})})();(function(){var a=document.querySelector(\".container .container-small h2\");a\u0026\u0026a.addEventListener(\"click\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"Top Banner Click\",eventLabel:a.currentTarget.textContent})})})();(function(){var a=document.querySelector(\"div.post-vote \\x3e .vote-up\");\na\u0026\u0026a.addEventListener(\"click\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"Thumbs Up\",eventLabel:",["escape",["macro",8],8,16],"})})})()});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":64
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Ega\u0026\u0026ga(function(){if(ga.getAll()){var a=ga.getAll();if(a)for(var b=0;b\u003Ca.length;b++)a[b]\u0026\u0026(trackerName=a[b].get(\"name\"),clientId=a[b].get(\"clientId\"),\"UA-43330364-7\"==a[b].get(\"trackingId\")?ga(trackerName+\".set\",{dimension3:clientId,dimension1:",["escape",["macro",31],8,16],",dimension4:",["escape",["macro",5],8,16],"}):\"UA-43330364-1\"==a[b].get(\"trackingId\")\u0026\u0026ga(trackerName+\".set\",{dimension16:clientId,dimension17:",["escape",["macro",5],8,16],"}),ga(trackerName+\".send\",\"event\",\"service\",\"clientId\",{nonInteraction:!0}))}});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":68
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E$\u0026\u0026$(\".block-selector\")\u0026\u0026$(\".block-selector\").change(function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Selector Changed\",eventLabel:a.target.value})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":70
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){var a=document.getElementById(\"mktoForm_1539\");a\u0026\u0026a.addEventListener(\"submit\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Forms\",eventAction:\"Sumbit\",eventLabel:\"Like our stories? We have more!\"})})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":71
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a,b,c,d){dataLayer.push({event:\"gaEvent\",eventCategory:a,eventAction:b,eventLabel:c,eventValue:d});return!1}window.addEventListener(\"akinator\",function(a){if(a.detail){var b=null===a.detail.action?\"finished\":a.detail.action;sendEvent(\"Akinator\",a.detail.text,b,Number(a.detail.isResult))}});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":72
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"button.pressed\",function(a){if(a.detail){var b=\"i am - \"+a.detail.iam+\" | doing - \"+a.detail.doing+\" | interested - \"+a.detail.interested+\" | text - \"+a.detail.replacementText;console.log(a.detail);dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Selector Changed\",eventLabel:b})}});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":73
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cimg src=\"https:\/\/trc.taboola.com\/taboolaaccount-ireneteamwrikecom\/log\/3\/mark?marking-type=okr-remark\u0026amp;item-url=",["escape",["macro",6],12],"\" width=\"0\" height=\"0\"\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":74
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Efunction createPixel(b){var a=document.createElement(\"img\");a.height=0;a.width=0;a.src=b;document.body.appendChild(a)}function sendGaEvent(b){dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Submit\",eventLabel:b})}\n\"ebook_form__submitted\"==",["escape",["macro",14],8,16],"?(createPixel(\"https:\/\/www.facebook.com\/tr?id\\x3d307980792700798\\x26ev\\x3debook_form__submitted\"),sendGaEvent(\"Ebook Form Submitted\")):\"webinar_form__submitted\"==",["escape",["macro",14],8,16],"?(createPixel(\"https:\/\/www.facebook.com\/tr?id\\x3d307980792700798\\x26ev\\x3dwebinar_form__submitted\"),sendGaEvent(\"Webinar Registration\")):\"template_form__submitted\"==",["escape",["macro",14],8,16],"\u0026\u0026(createPixel(\"https:\/\/www.facebook.com\/tr?id\\x3d307980792700798\\x26ev\\x3dtemplate_form__submitted\"),\nsendGaEvent(\"Template Form Submit\"));\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":75
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cimg height=\"1\" width=\"1\" style=\"display:none\" src=\"https:\/\/www.facebook.com\/tr?id=307980792700798\u0026amp;ev=CalendarButtonClick\"\u003E\n\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Click\",eventLabel:\"CalendarButton\"});\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":77
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"productivity_tour_form_submit\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Submit\",eventLabel:\"Productivity Tour\"});var a=document.createElement(\"img\");a.height=0;a.width=0;a.src=\"https:\/\/www.facebook.com\/tr?id\\x3d307980792700798\\x26ev\\x3dProductivityTourRegistration\";document.body.appendChild(a)});\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":78
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function a(){0\u003Cdocument.querySelectorAll(\"[role\\x3ddialog]\").length\u0026\u0026!b\u0026\u0026(dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Click\",eventLabel:\"Integrate with Google\"}),b=!0);setTimeout(a,1E3)}var b=!1;a()})();\u003C\/script\u003E\n\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":80
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":" \u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"new ROI page\",eventLabel:a})}window.addEventListener(\"input.update\",function(a){sendEvent(a.detail)});window.addEventListener(\"contact.click\",function(a){sendEvent(\"Contact us click\")});window.addEventListener(\"pdf.click\",function(a){sendEvent(\"Download pdf\")});window.addEventListener(\"toggle.beforeafter\",function(a){sendEvent(\"Toogle Chart\")});window.addEventListener(\"sharelink\",function(a){sendEvent(\"Share link click\")});\nwindow.addEventListener(\"sharelink\",function(a){sendEvent(\"Share link click\")});window.addEventListener(\"calculatepopup.contact.click\",function(a){sendEvent(\"Calculation popup contact us click in\")});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":82
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"cnlform.submitted\",function(a){dataLayer.push({event:a.type})});window.addEventListener(\"exitmodal.submitted\",function(a){dataLayer.push({event:a.type})});window.addEventListener(\"ebookform.submitted\",function(a){dataLayer.push({event:a.type})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":84
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a,b){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:a,eventLabel:b})}window.addEventListener(\"IMAGE_PROOFING_ACTION\",function(a){sendEvent(\"Image Proofing\",a.detail)});window.addEventListener(\"VIDEO_PROOFING_ACTION\",function(a){sendEvent(\"Video Proofing\",a.detail)});window.addEventListener(\"DOCUMENT_PROOFING_ACTION\",function(a){sendEvent(\"Document Proofing\",a.detail)});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":86
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){var a=document.querySelectorAll(\".shinypic\");if(0\u003Ca.length)for(var b=0;b\u003Ca.length;b++)a[b].addEventListener(\"click\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Banner Click\",eventLabel:document.location.pathname})})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":89
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function r(a){var c=[],b;for(b in a)if(a.hasOwnProperty(b)\u0026\u0026\"function\"!==typeof a[b]){var d=encodeURIComponent(b)+\"\\x3d\";d=\"object\"===typeof a[b]?d+encodeURIComponent(JSON.stringify(a[b])):d+encodeURIComponent(a[b]);c.push(d)}return c.join(\"\\x26\")}function h(a){this.storageKey=a;window.addEventListener(\"beforeunload\",function(){f.eraseCookie(self.storageKey+this.sessionPostfix)})}function e(a){this.key=a;this.lc=new h(this.key)}var p=\"isr\",t=\"sc\",u=\"www.wrike.com\",v=\"UA-43330364-7\",g=\n\"_crp _edu _pr _free _cp _zd _hm _prc _pmc _hlp _fre _mce _win _mac _chrome _firefox _safari _ie _en _mobile _competitor\".split(\" \"),l=.060158599,w={_crp:2.32942009,_edu:-.776422024,_pr:.078331299,_free:-1.434470057,_cp:1.064800024,_zd:1.054970026,_hm:.629745007,_prc:.311055005,_pmc:.296287,_hlp:.841852009,_fre:-.131555006,_mce:.411868989,_win:.0837363,_mac:.422865003,_chrome:-.137813002,_firefox:-.276980996,_safari:-.213057995,_ie:-.096017301,_en:.411338001,_mobile:-.295780987,_competitor:-.223602995},\nk=[1.26164362E-11,29.079418,.10610204,4.74431314],q=-2.915994644,n=[{countries:\"AO BF BI BJ BW CD CF CG CI CM CV DJ ER ET GA GH GM GN GQ GW KE KM LR LS LY MG ML MR MU MW MZ NA NE NG RE RW SC SL SN SO SS ST SZ TD TG TZ UG YT ZM ZW\".split(\" \"),score:-3.412337065},{countries:\"BD BT FM MH NP NR NU PK PW TO\".split(\" \"),score:-9.999995232},{countries:\"AF CN IN LK MN MP MV PF TW\".split(\" \"),score:-3.403633595},{countries:[\"BR\"],score:-3.027284861},{countries:\"AL BA BG GR HR ME MK PL RO RS XK\".split(\" \"),\nscore:-2.975222826},{countries:\"CY CZ EE HU LT LV SI SK\".split(\" \"),score:-2.691224098},{countries:[\"AT\",\"CH\",\"DE\",\"LI\"],score:-2.115684748},{countries:[\"AD\",\"DZ\",\"MA\",\"TN\"],score:-3.134845257},{countries:[\"BE\",\"FR\",\"LU\",\"MC\"],score:-2.104984283},{countries:[\"IT\",\"SM\"],score:-2.271875858},{countries:[\"HK\",\"JP\",\"KR\",\"MO\",\"SG\"],score:-2.199761629},{countries:\"AI AR BL BO BQ BS CO CR CU DM DO EC FK GD GF GP GT GY HT JM KI LC MF MS NI PA PE PR PY SR SV SX TC TV UY VC VE\".split(\" \"),score:-2.793750048},\n{countries:\"AG AW BB BZ CL CW HN KN MQ MX TT VI\".split(\" \"),score:-2.476722479},{countries:\"BH EG IQ IR JO KW LB OM PS QA SD SY TR YE\".split(\" \"),score:-3.444497347},{countries:[\"AE\",\"SA\"],score:-1.678791404},{countries:[\"BM\",\"CA\",\"KY\"],score:-2.037349939},{countries:[\"US\"],score:-1.449002385},{countries:[\"AX\",\"FI\",\"IS\",\"NL\"],score:-2.165637732},{countries:[\"DK\",\"FO\",\"GL\",\"NO\",\"SE\"],score:-1.974584579},{countries:\"AS AU CK FJ IO NC NZ SB VU WS\".split(\" \"),score:-1.852559566},{countries:\"AM BY GE KG KZ MD TJ TM UA UZ\".split(\" \"),\nscore:-3.194761753},{countries:[\"AZ\",\"RU\"],score:-2.779341459},{countries:[\"BN\",\"ID\",\"PG\",\"TL\",\"VN\"],score:-2.541050196},{countries:\"GU KH LA MM MY PH TH\".split(\" \"),score:-2.733840466},{countries:[\"ES\",\"PT\"],score:-2.566735268},{countries:\"GG IE IL MT VG ZA\".split(\" \"),score:-2.556703568},{countries:[\"GB\",\"GI\",\"IM\",\"JE\"],score:-1.909969211}],f={createCookie:function(a,c,b){if(b){var d=new Date;d.setTime(d.getTime()+864E5*b);b=\"; expires\\x3d\"+d.toGMTString()}else b=\"\";document.cookie=a+\"\\x3d\"+c+b+\n\"; path\\x3d\/; domain\\x3dwrike.com\"},readCookie:function(a){a+=\"\\x3d\";for(var c=document.cookie.split(\";\"),b=0;b\u003Cc.length;b++){for(var d=c[b];\" \"==d.charAt(0);)d=d.substring(1,d.length);if(0==d.indexOf(a))return d.substring(a.length,d.length)}return null},eraseCookie:function(a){f.createCookie(a,\"\",-1)},isIFrame:function(){try{return window.self!==window.top}catch(a){return!0}},currentUrlContains:function(a){return-1\u003Cwindow.location.pathname.search(a)},getQueryVariable:function(a){var c=window.location.search.substring(1);\nc=c.split(\"\\x26\");for(var b=0;b\u003Cc.length;b++){var d=c[b].split(\"\\x3d\");if(decodeURIComponent(d[0])==a)return decodeURIComponent(d[1])}}};h.prototype.sessionPostfix=\"-session\";h.prototype.set=function(a,c,b){var d=b?this.storageKey+this.sessionPostfix:this.storageKey,e=f.readCookie(d);data=e?JSON.parse(e):{};data[a]=c;f.eraseCookie(d);f.createCookie(d,JSON.stringify(data),b?0:365)};h.prototype.get=function(a,c){var b=c?this.storageKey+this.sessionPostfix:this.storageKey;if(b=f.readCookie(b))return data=\nJSON.parse(b),data[a]};h.prototype.getAll=function(a){a=a?this.storageKey+this.sessionPostfix:this.storageKey;return data=(a=f.readCookie(a))?JSON.parse(a):{}};h.prototype.getAllWithSession=function(){var a=this.getAll(),c=this.getAll(!0),b;for(b in c)c.hasOwnProperty(b)\u0026\u0026(a[b]=c[b]);return a};e.prototype.updateVisitedPages=function(){if(!f.isIFrame()){var a=this.lc.get(\"_pg\");-1===document.location.href.indexOf(\"\/resend\/\")\u0026\u0026this.lc.set(\"_pg\",a?a+1:1)}};e.prototype.specificPageVisit=function(){f.currentUrlContains(\/^\\\/([a-z]{2}(-[a-z]{2})?\\\/)?([a-z]{2}\\\/)?$\/)\u0026\u0026\n-1===window.location.pathname.indexOf(\"\/hc\")\u0026\u0026this.lc.set(\"_hm\",!0);f.currentUrlContains(\/(price|pricing)\/)\u0026\u0026this.lc.set(\"_pr\",!0);f.currentUrlContains(\/(-vs-|competitors|alternative|trello|basecamp|smartsheet)\/)\u0026\u0026this.lc.set(\"_cmp\",!0);f.currentUrlContains(\/(project|manage|collab|tour|blog)\/)\u0026\u0026this.lc.set(\"_pmc\",!0);f.currentUrlContains(\/(\\\/hc|\\\/help|webinar)\/)\u0026\u0026this.lc.set(\"_hlp\",!0);f.currentUrlContains(\/(free)\/)\u0026\u0026this.lc.set(\"_fre\",!0);f.currentUrlContains(\/(marketing|creativ|enterprise)\/)\u0026\u0026-1===\nwindow.location.pathname.indexOf(\"\/embed\")\u0026\u0026this.lc.set(\"_mce\",!0)};e.prototype.detectOS=function(){-1===window.navigator.appVersion.indexOf(\"Mobile\")\u0026\u0026(-1!==window.navigator.appVersion.indexOf(\"Win\")?this.lc.set(\"_win\",!0):-1!==window.navigator.appVersion.indexOf(\"Mac\")\u0026\u0026this.lc.set(\"_mac\",!0))};e.prototype.detectBrowser=function(){var a=navigator.userAgent;\/(mobile|samsung|opera|edge|facebook|yandex|android|pinterest|uc\\sbrowser|chromium|vivaldi)\/.test(a.toLowerCase())||(-1!==a.indexOf(\"MSIE\")||\n0\u003Ca.indexOf(\"Trident\/\")?this.lc.set(\"_ie\",!0):-1!==a.indexOf(\"Chrome\")?this.lc.set(\"_chrome\",!0):-1!==a.indexOf(\"Safari\")?this.lc.set(\"_safari\",!0):-1!==a.indexOf(\"Firefox\")\u0026\u0026this.lc.set(\"_firefox\",!0))};e.prototype.detectLang=function(){var a=navigator.languages?navigator.languages[0]:navigator.language||navigator.userLanguage;0===a.indexOf(\"en\")\u0026\u0026this.lc.set(\"_en\",!0)};e.prototype.detectDeviceType=function(){var a=navigator.userAgent,c=\/(iphone|ipad|android|j2me|mobile|tablet|opera mini|iemobile|edgios|edga|opera mobi)\/ig;\nc.test(a)\u0026\u0026this.lc.set(\"_mobile\",!0)};e.prototype.chatConv=function(){if(null==this.lc.get(\"_zd\")){var a=this;if(window.SnapABug){var c=SnapABug.submitForm.bind(SnapABug);SnapABug.submitForm=function(){a.lc.set(\"_zd\",!0);return c()};var b=SnapABugChat.submitChat.bind(SnapABugChat);SnapABugChat.submitChat=function(){a.lc.set(\"_zd\",!0);return b()}}var d=document.querySelector('[action\\x3d\"\/hc\/ru\/requests\"]');d\u0026\u0026d.addEventListener(\"submit\",function(c){a.lc.set(\"_zd\",!0)})}};e.prototype.pricingRegister=\nfunction(){for(var a=document.querySelectorAll(\"[data-signup-form]\"),c=document.querySelectorAll(\".wrike-lp-form\"),b=0;b\u003Ca.length;b++)a[b].addEventListener(\"submit\",this.singupSubmit.bind(this));for(b=0;b\u003Cc.length;b++)c[b].parentElement.addEventListener(\"submit\",this.singupSubmit.bind(this))};e.prototype.competitorCampaign=function(){var a=f.getQueryVariable(\"ga_campaign\")||f.getQueryVariable(\"utm_campaign\"),c=f.getQueryVariable(\"utm_referrer\"),b=\/(competitor)\/ig;(a\u0026\u0026b.test(a)||c\u0026\u0026b.test(c))\u0026\u0026this.lc.set(\"_competitor\",\n!0,!0)};e.prototype.singupSubmit=function(a){var c=a.target;a=c.getAttribute(\"data-plan-id\")||\"pro\";f.currentUrlContains(\/(price|pricing)\/)\u0026\u0026this.lc.set(\"_prc\",!0);var b=\/\\.edu\/;c=c.querySelector('[name\\x3d\"email\"]');b.test(c.value.split(\"@\").pop())\u0026\u0026this.lc.set(\"_edu\",!0);switch(a){case \"free\":this.lc.set(\"_free\",!0);break;case \"pro-price\":case \"pro\":this.lc.set(\"_free\",void 0);this.lc.set(\"_cp\",void 0);break;default:this.lc.set(\"_cp\",!0)}};e.prototype.isCorporate=function(){this.lc.set(\"_crp\",",["escape",["macro",4],8,16],"\u0026\u0026\n\"corporate\"===",["escape",["macro",4],8,16],")};e.prototype.setSources=function(){if(null==this.lc.get(\"_dr\",!0)\u0026\u0026null==this.lc.get(\"_cpt\",!0)){var a=!1,c;ga\u0026\u0026ga(function(){var a=ga.getAll();if(a)for(var b=0;b\u003Ca.length;b++)if(a[b]\u0026\u0026a[b].get(\"trackingId\")===v){c=a[b];break}});if(c){var b=f.getQueryVariable(\"utm_referrer\");b=b\u0026\u0026-1\u003Cb.indexOf(\"wrike.com\");var d=f.getQueryVariable(\"ga_campaign\")||f.getQueryVariable(\"utm_campaign\"),e=c.get(\"_utmz\")\u0026\u0026-1\u003Cc.get(\"_utmz\").indexOf(\"utmcsr\\x3d(direct)\"),g=!c.get(\"referrer\");\n!d\u0026\u0026(b||e)\u0026\u0026g\u0026\u0026(a=!0)}this.lc.set(\"_dr\",a,!0)}};e.prototype.getScoreByCountryCode=function(a){if(!a)return q;for(var c=0;c\u003Cn.length;c++)if(-1\u003Cn[c].countries.indexOf(a))return n[c].score;return q};e.prototype.getCountryScore=function(){var a=f.readCookie(\"country\");if(!a)return this.getScoreByCountryCode();a=a.split(\":\").shift();return this.getScoreByCountryCode(a)};e.prototype.runScoring=function(){for(var a=\"updateVisitedPages specificPageVisit detectOS detectBrowser detectLang chatConv pricingRegister setSources isCorporate detectDeviceType competitorCampaign\".split(\" \"),\nc=0;c\u003Ca.length;c++)this[a[c]]()};e.prototype.getScoreByFlag=function(a){return w[a]||0};e.prototype.debugLog=function(){var a=this.lc.getAllWithSession();console.log(a._pg);for(var c=0;c\u003Cg.length;c++)console.log(g[c],a[g[c]]);(a=f.readCookie(\"country\"))\u0026\u0026console.log(\"country\",a.split(\":\").shift())};e.prototype.calculateScore=function(){var a=this.lc.getAllWithSession();6\u003Ca._pg\u0026\u0026(a._pg=6);for(var c=0,b=[],d=0;d\u003Cg.length;d++)a[g[d]]\u0026\u0026(c+=this.getScoreByFlag(g[d]),b.push({flag:g[d],value:this.getScoreByFlag(g[d])}));\nc+=a._pg*l;b.push({flag:\"_pg\",value:a._pg*l});b.push({flag:\"country\",value:this.getCountryScore()});c+=this.getCountryScore();a=1\/(1+Math.exp(-c));a=k[0]*Math.exp(k[1]*a)+k[2]*Math.exp(k[3]*a);dataLayer.push({emrr:parseFloat(Number(a).toFixed(3)),event:\"emrr\"});dataLayer.push({emrr12:parseFloat(Number(12*a).toFixed(3)),event:\"emrr12\"});return a};e.prototype.sendStats=function(){var a=this.lc.getAllWithSession();6\u003Ca._pg\u0026\u0026(a._pg=6);for(var c=0,b={},d=0;d\u003Cg.length;d++)a[g[d]]\u0026\u0026(c+=this.getScoreByFlag(g[d]),\nb[g[d]]=this.getScoreByFlag(g[d]));c+=a._pg*l;b._pg=a._pg*l;a=f.readCookie(\"country\");b.country=a?a.split(\":\").shift():\"--\";a=this.getCountryScore();b.country_value=a;c+=a;c=1\/(1+Math.exp(-c));c=k[0]*Math.exp(k[1]*c)+k[2]*Math.exp(k[3]*c);b.emrr=Number(c).toFixed(3);var e=new XMLHttpRequest;b={events:[{group:\"registration\",event:\"registration_scor\",path:window.location.pathname,value:b}]};b=r(b);e.open(\"POST\",\"https:\/\/\"+u+\"\/stat\/site\",!0);e.setRequestHeader(\"Content-type\",\"application\/x-www-form-urlencoded\");\ne.setRequestHeader(\"Wrike-Client-Id\",\"site- \"+(new Date).getTime());var h=this;e.onreadystatechange=function(){4===e.readyState\u0026\u0026200===e.status\u0026\u0026h.lc.set(p,!0)};e.send(b)};var m=new e(t);ga(function(){m.lc.get(p)||(m.runScoring(),m.calculateScore(),-1\u003Cwindow.location.pathname.indexOf(\"\/resend\/\")\u0026\u0026m.sendStats())})})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":92
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"comparison.dragged\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Before\/After dragged\"})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":94
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar banner;function writeLog(c){return function(a){if(a.detail.task){var b=\"Task:\"+a.detail.task;a.detail.board\u0026\u0026(b=\"Task:\"+a.detail.task+\", Board: \"+a.detail.board)}else a.detail.status\u0026\u0026(b=a.detail.status);dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:c,eventLabel:b})}}window.addEventListener(\"IS_BOARDS__TASK_MOVED\",writeLog(\"Boards: task moved\"));window.addEventListener(\"IS_CUSTOM_WORKFLOW__STATUS_CHANGED\",writeLog(\"Custom workflow: status changed\"));\nwindow.addEventListener(\"IS_GANTT_CHART__TASK_MOVED\",writeLog(\"Gantt chart: task moved\"));window.addEventListener(\"IS_GANTT_CHART__TASK_RESIZED\",writeLog(\"Gantt chart: task resized\"));window.addEventListener(\"IS_REPORT__STATUS_STATE_CHANGED\",writeLog(\"Report: status state changed\"));(banner=document.querySelector(\"#lp-code-826 a\"))\u0026\u0026banner.addEventListener(\"click\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Banner Click\",eventLabel:\"To Demo Form\"})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":97
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a,b,c){dataLayer.push({event:\"gaEvent\",eventCategory:a,eventAction:b,eventLabel:c})}window.addEventListener(\"DEADLINE_CHECKED\",function(a){sendEvent(\"Train Brain Page\",\"Result\",a.detail.reason)});window.addEventListener(\"TIMELINE__TASK_MOVED\",function(a){sendEvent(\"Train Brain Page\",\"Task Moved\",a.detail.task)});window.addEventListener(\"TIMELINE__TASK_RESIZED\",function(a){sendEvent(\"Train Brain Page\",\"Task Resized\",a.detail.task)});\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":99
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"load\",function(){var a=\"undefined\";HelpCenter.user.role\u0026\u0026HelpCenter.user\u0026\u0026HelpCenter\u0026\u0026(a=HelpCenter.user.role);window.SnapEngage\u0026\u0026SnapEngage.setCallback(\"StartChat\",function(b,c,d){dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:\"Chat Started\",eventLabel:a})})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":102
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"game.finished\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Games\",eventAction:\"Halloween Game\",eventLabel:\"Finished\"})});window.addEventListener(\"game.start\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Games\",eventAction:\"Halloween Game\",eventLabel:\"Started\"})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":103
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"SA_SURVEY.ANSWER\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Self Assessment Survey\",eventAction:a.detail.question,eventLabel:a.detail.answer})});window._saSurvey\u0026\u0026window._saSurvey.stage\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Self Assessment Survey\",eventAction:\"Result\",eventLabel:window._saSurvey.stage});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":106
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function f(a){var d=[],b;for(b in a)if(a.hasOwnProperty(b)\u0026\u0026\"function\"!==typeof a[b]){var c=encodeURIComponent(b)+\"\\x3d\";c=\"object\"===typeof a[b]?c+encodeURIComponent(JSON.stringify(a[b])):c+encodeURIComponent(a[b]);d.push(c)}return d.join(\"\\x26\")}function g(a,d,b){a=new XMLHttpRequest;var c=f(e);a.onreadystatechange=function(){4==this.readyState\u0026\u0026200==this.status?\"function\"===typeof d\u0026\u0026d():\"function\"===typeof b\u0026\u0026b(this.status)};a.withCredentials=!0;a.open(\"POST\",\"https:\/\/www.wrike.com\/stat\/site\",\n!0);a.setRequestHeader(\"Content-type\",\"application\/x-www-form-urlencoded\");a.setRequestHeader(\"Wrike-Client-Id\",\"site- \"+(new Date).getTime());a.send(c)}var e={events:[{group:\"interactions\",event:\"page__scrolled\",path:window.location.pathname,value:{percent:",["escape",["macro",23],8,16],"}}]};g(e)})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":109
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function f(a){var d=[],b;for(b in a)if(a.hasOwnProperty(b)\u0026\u0026\"function\"!==typeof a[b]){var c=encodeURIComponent(b)+\"\\x3d\";c=\"object\"===typeof a[b]?c+encodeURIComponent(JSON.stringify(a[b])):c+encodeURIComponent(a[b]);d.push(c)}return d.join(\"\\x26\")}function g(a,d,b){a=new XMLHttpRequest;var c=f(e);a.onreadystatechange=function(){4==this.readyState\u0026\u0026200==this.status?\"function\"===typeof d\u0026\u0026d():\"function\"===typeof b\u0026\u0026b(this.status)};a.withCredentials=!0;a.open(\"POST\",\"https:\/\/www.wrike.com\/stat\/site\",\n!0);a.setRequestHeader(\"Content-type\",\"application\/x-www-form-urlencoded\");a.setRequestHeader(\"Wrike-Client-Id\",\"site- \"+(new Date).getTime());a.send(c)}var e={events:[{group:\"interactions\",event:\"link__click\",path:\"library.wrike.com\"==window.location.hostname?window.location.pathname.match(\"(\/[^\/]*\/).*\")[1]:window.location.pathname,value:{click_url:",["escape",["macro",32],8,16],",click_target:",["escape",["macro",33],8,16],",click_text:",["escape",["macro",34],8,16],",click_id:",["escape",["macro",35],8,16],",click_class:",["escape",["macro",36],8,16],"}}]};\ng(e)})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":110
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar events=[\"ebook_form__submitted\",\"webinar_form__submitted\",\"template_form__submitted\",\"pm_guide_form__submitted\"];events.forEach(function(a){window.addEventListener(a,function(){dataLayer.push({event:a})})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":111
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"cariers_form__subbmitted\",function(){dataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Submit\",eventLabel:\"Cariers Form Subbmitted\"})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":113
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Efunction createPixel(b){var a=document.createElement(\"img\");a.height=0;a.width=0;a.src=b;document.body.appendChild(a)}\"ebook_form__submitted\"==",["escape",["macro",14],8,16],"\u0026\u0026(createPixel(\"https:\/\/analytics.twitter.com\/i\/adsct?txn_id\\x3dnz8xq\\x26p_id\\x3dTwitter\\x26tw_sale_amount\\x3d0\\x26tw_order_quantity\\x3d0\"),createPixel(\"\/\/t.co\/i\/adsct?txn_id\\x3dnz8xq\\x26p_id\\x3dTwitter\\x26tw_sale_amount\\x3d0\\x26tw_order_quantity\\x3d0\"));\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":114
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Efunction createPixel(b){var a=document.createElement(\"img\");a.height=0;a.width=0;a.src=b;document.body.appendChild(a)}\"ebook_form__submitted\"==",["escape",["macro",14],8,16],"?createPixel(\"https:\/\/dc.ads.linkedin.com\/collect\/?pid\\x3d114863\\x26conversionId\\x3d335337\\x26fmt\\x3dgif\"):\"webinar_form__submitted\"==",["escape",["macro",14],8,16],"\u0026\u0026createPixel(\"https:\/\/dc.ads.linkedin.com\/collect\/?pid\\x3d114863\\x26conversionId\\x3d335345\\x26fmt\\x3dgif\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":115
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"interview_slider__switched\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Slider Swithed (David page)\",eventLabel:a.detail.name})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":116
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.uetq=window.uetq||[];window.uetq.push({ec:\"conversion\",ea:\"form_submit\",el:\"trial_with_threshold\",ev:",["escape",["macro",20],8,16],"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":119
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.uetq=window.uetq||[];window.uetq.push({ec:\"conversion\",ea:\"form_submit\",el:\"trial_corporate_email\"});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":120
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript id=\"term-e7e5d07437489\" data-gtmsrc=\"https:\/\/vidassets.terminus.services\/a3816217-2e51-4af0-ad1b-7bc624e02e1f\/t.js\" type=\"text\/gtmscript\"\u003E\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":121
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a,b){dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:a,eventLabel:b})}window.addEventListener(\"fiveminFirstlink\",function(a){sendEvent(\"HC PopUp Link Click\",\"Ask the Wrike Community\")});window.addEventListener(\"tenminFirstlink\",function(a){sendEvent(\"Community PopUp Link Click\",\"Sign-in with Wrike\")});window.addEventListener(\"tenminSecondlink\",function(a){sendEvent(\"Community PopUp Link Click\",\"Start a free Wrike Trial\")});\nwindow.addEventListener(\"tenminPopup\",function(a){sendEvent(\"Community PopUp Shown\")});window.addEventListener(\"fiveminPopup\",function(a){sendEvent(\"HC PopUp Shown\")});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":125
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar bfId=\"RMNuOhvlXHjo8OFM\",bfSession=.041666666666666664;(function(){var a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=(\"https:\"===document.location.protocol?\"https:\/\/\":\"http:\/\/\")+\"munchkin.brightfunnel.com\/js\/build\/bf-munchkin.min.js\";var b=document.getElementsByTagName(\"script\")[0];b.parentNode.insertBefore(a,b)})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":126
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar bF=document.createElement(\"div\");bF.innerHTML=\"\\x3cbrightfunnel\\x3e\\x3c\/brightfunnel\\x3e\";document.body.appendChild(bF);\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":129
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"live_demo__interaction\",function(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Live Demo Interaction\",eventAction:a.detail.action,eventLabel:a.detail.step})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":130
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function f(a){var d=[],b;for(b in a)if(a.hasOwnProperty(b)\u0026\u0026\"function\"!==typeof a[b]){var c=encodeURIComponent(b)+\"\\x3d\";c=\"object\"===typeof a[b]?c+encodeURIComponent(JSON.stringify(a[b])):c+encodeURIComponent(a[b]);d.push(c)}return d.join(\"\\x26\")}function g(a,d,b){a=new XMLHttpRequest;var c=f(e);a.onreadystatechange=function(){4==this.readyState\u0026\u0026200==this.status?\"function\"===typeof d\u0026\u0026d():\"function\"===typeof b\u0026\u0026b(this.status)};a.withCredentials=!0;a.open(\"POST\",\"https:\/\/www.wrike.com\/stat\/site\",\n!0);a.setRequestHeader(\"Content-type\",\"application\/x-www-form-urlencoded\");a.setRequestHeader(\"Wrike-Client-Id\",\"site- \"+(new Date).getTime());a.send(c)}var e={events:[{group:\"interactions\",event:\"page__scrolled\",path:window.location.pathname.match(\"(\/[^\/]*\/).*\")[1],value:{percent:",["escape",["macro",23],8,16],"}}]};g(e)})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":131
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEvent(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Library Ebook\",eventAction:\"Social Sharing\",eventLabel:a})}var socialButton=",["escape",["macro",37],8,16],";void 0!=socialButton?(\"icon sprite\"==",["escape",["macro",36],8,16],"\u0026\u0026(socialButton=socialButton.parentElement),sendEvent(socialButton.dataset.tp)):console.error(\"Failed to read social button click\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":132
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E_airpr={id:\"665839\",ga_dim:\"10\",ga_account_preset:\"UA-43330364-7\"};(function(c,d,a,b){a=c.createElement(d);a.async=1;a.src=\"\/\/px.airpr.com\/airpr.js\";b=c.getElementsByTagName(d)[0];b.parentNode.insertBefore(a,b)})(document,\"script\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":133
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction json2urlencoded(a){var d=[],b;for(b in a)if(a.hasOwnProperty(b)\u0026\u0026\"function\"!==typeof a[b]){var c=encodeURIComponent(b)+\"\\x3d\";c=\"object\"===typeof a[b]?c+encodeURIComponent(JSON.stringify(a[b])):c+encodeURIComponent(a[b]);d.push(c)}return d.join(\"\\x26\")}\nfunction sendData(a,d,b){var c=new XMLHttpRequest;a=json2urlencoded(a);c.onreadystatechange=function(){4==this.readyState\u0026\u0026200==this.status?\"function\"===typeof d\u0026\u0026d():\"function\"===typeof b\u0026\u0026b(this.status)};c.withCredentials=!0;c.open(\"POST\",\"https:\/\/www.wrike.com\/stat\/site\",!0);c.setRequestHeader(\"Content-type\",\"application\/x-www-form-urlencoded\");c.setRequestHeader(\"Wrike-Client-Id\",\"site- \"+(new Date).getTime());c.send(a)}\nfunction sendEventToLog(a,d){var b={events:[{group:\"interactions\",event:a,path:window.location.pathname,value:{plan:d}}]};sendData(b)}function sendEventToGa(a){dataLayer.push({event:\"gaEvent\",eventCategory:\"Page elements\",eventAction:\"Pricing page ROI calculator\",eventLabel:a})}window.addEventListener(\"calculator.open\",function(a){sendEventToGa(\"Calculator opened\");sendEventToLog(\"roi__calc_opened_pricing_page\",\"(none)\")});\nwindow.addEventListener(\"input.update\",function(a){sendEventToGa(a.detail+\" input updated\")});window.addEventListener(\"plan.recommended\",function(a){sendEventToLog(\"roi__calc_plan_recommended\",a.detail)});window.addEventListener(\"trialForm.open\",function(a){sendEventToLog(\"roi__calc_plan_form_opened\",a.detail)});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":134
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\n\u003Cscript type=\"text\/gtmscript\"\u003Evar yahoo_conversion_id=1000988783,yahoo_conversion_label=",["escape",["macro",38],8,16],",yahoo_conversion_value=\"undefined\"!==typeof ",["escape",["macro",20],8,16],"?",["escape",["macro",20],8,16],":0;\u003C\/script\u003E\n\u003Cscript type=\"text\/gtmscript\" data-gtmsrc=\"https:\/\/s.yimg.jp\/images\/listing\/tool\/cv\/conversion.js\"\u003E\u003C\/script\u003E\n\u003Cnoscript\u003E\n    \u003Cdiv style=\"display:inline;\"\u003E\n        \u003Cimg height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"https:\/\/b91.yahoo.co.jp\/pagead\/conversion\/1000988783\/?value=0\u0026amp;label=",["escape",["macro",38],12],"\u0026amp;guid=ON\u0026amp;script=0\u0026amp;disvt=true\"\u003E\n    \u003C\/div\u003E\n\u003C\/noscript\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":135
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"techScroll\",percent:",["escape",["macro",23],8,16],"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":141
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow.addEventListener(\"spaNewPageLoaded\",function(a){dataLayer.push({event:\"technicalReload\",url:a.detail.virtualUrl,path:a.detail.virtualPath})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":143
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar campaign=",["escape",["macro",39],8,16],"||",["escape",["macro",40],8,16],"||\"\";if(-1!=campaign.toLowerCase().indexOf(\"brand\")){var d=new Date;d.setTime(d.getTime()+36E5);document.cookie=\"WrikeBrandedTraffic\\x3dtrue;expires\\x3d\"+d.toUTCString()+\";domain\\x3d.wrike.com;path\\x3d\/\"};\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":145
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var a=",["escape",["macro",32],8,16],"||\"\";",["escape",["macro",32],8,16],".endsWith(\".pdf\")\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Link Clicks\",eventAction:\"File Downloaded\",eventLabel:a})})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":148
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var b=",["escape",["macro",32],8,16],"||\"\",a=",["escape",["macro",36],8,16],"||\"\",c=[\"\/wrike-creative-teams\/\",\"\/wrike-marketers-teams\/\"];\"card__link\"!=a\u0026\u0026-1==a.indexOf(\"td-cta__button\")||-1==c.indexOf(document.location.pathname)||dataLayer.push({event:\"gaEvent\",eventCategory:\"Link Clicks\",eventAction:\"Traina Content Links\",eventLabel:b})})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":149
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003E(function(){function b(b){var a=document.createElement(\"img\");a.height=1;a.width=1;a.style=\"display:none\";a.src=b;document.body.appendChild(a)}var d=",["escape",["macro",32],8,16],"||\"\",c=",["escape",["macro",36],8,16],"||\"\",e=[\"\/lg-where-work-flows-creative\/\",\"\/lg-where-work-flows-marketer\/\"];\"card__link\"!=c\u0026\u0026-1==c.indexOf(\"td-cta__button\")||-1==e.indexOf(document.location.pathname)||(b(\"https:\/\/dc.ads.linkedin.com\/collect\/?pid\\x3d114863\\x26conversionId\\x3d776569\\x26fmt\\x3dgif\"),b(\"https:\/\/www.facebook.com\/tr?id\\x3d307980792700798\\x26ev\\x3dclick_on_the_asset_-_where_work_flows\"),\ndataLayer.push({event:\"gaEvent\",eventCategory:\"Click on the asset\",eventAction:\"Where work flows\",eventLabel:d}))})();\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":150
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar pageviewSuffix=\" page viewed\";function sendEventToGa(a,b){dataLayer.push({event:\"gaEvent\",eventCategory:\"AirPR Actions\",eventAction:a,eventLabel:b})}\nswitch(!0){case \/^(\\\/((\\w{2})|(pt-br)))?\\\/([vw]\\w\\\/)?$\/.test(window.location.pathname):window.addEventListener(\"trial_form__submitted\",function(){sendEventToGa(\"Homepage trial start\",\"\")});window.addEventListener(\"non_trial_form__submitted\",function(a){content=a.detail.value.content||\"\";\"outro_demo\"==content\u0026\u0026sendEventToGa(\"Homepage demo request\",content)});break;case \/^(\\\/((\\w{2})|(pt-br)))?\\\/tour(-[wv]\\w)?\\\/.*$\/.test(window.location.pathname):sendEventToGa(\"Tour\"+pageviewSuffix,\"\");window.addEventListener(\"trial_form__submitted\",\nfunction(){sendEventToGa(\"Tour trial start\",\"\")});window.addEventListener(\"non_trial_form__submitted\",function(a){content=a.detail.value.content||\"\";\"outro_demo\"==content?sendEventToGa(\"Tour demo request\",content):\"watchdemo\"==content\u0026\u0026sendEventToGa(\"Tour watch demo request\",content)});break;case \/^(\\\/((\\w{2})|(pt-br)))?\\\/demo(-[wv]\\w)?\\\/.*$\/.test(window.location.pathname):sendEventToGa(\"Demo\"+pageviewSuffix);window.addEventListener(\"trial_form__submitted\",function(){sendEventToGa(\"Tour trial start\",\n\"\")});window.addEventListener(\"non_trial_form__submitted\",function(a){content=a.detail.value.content||\"\";\"outro_demo\"==content?sendEventToGa(\"Demo page demo request\",content):\"watchdemo\"==content\u0026\u0026sendEventToGa(\"Demo page watch demo request\",content)});break;case \/^(\\\/((\\w{2})|(pt-br)))?\\\/customers(-[wv]\\w)?\\\/.*$\/.test(window.location.pathname):var customer=window.location.pathname.split(\"\/\")[2];sendEventToGa(\"Customers\"+(\"\"==customer?\"\":\" \"+customer)+pageviewSuffix,\"\");window.addEventListener(\"trial_form__submitted\",\nfunction(){sendEventToGa(\"Customers\"+(\"\"==customer?\"\":\" \"+customer)+\" trial start\",\"\")});break;case \/^(\\\/((\\w{2})|(pt-br)))?\\\/price(-[wv]\\w)?\\\/$\/.test(window.location.pathname):sendEventToGa(\"Price\"+pageviewSuffix,\"\");window.addEventListener(\"trial_form__submitted\",function(a){sendEventToGa(\"Price \"+a.detail.value.registration_plan+\" trial start\",a.detail.value.registration_plan)});window.addEventListener(\"non_trial_form__submitted\",function(a){sendEventToGa(\"Price contact request\",a.detail.value.content)});\nbreak;case \/^(\\\/((\\w{2})|(pt-br)))?\\\/(marketing-project-management|creative-project-management|project-management|product-management-software|business-operations-teams|professional-services-management|team-collaboration-software)(-[wv]\\w)?\\\/$\/.test(window.location.pathname):solution=window.location.pathname.split(\"\/\").join(\"\");sendEventToGa(\"Solution \"+solution+pageviewSuffix,\"\");window.addEventListener(\"trial_form__submitted\",function(){sendEventToGa(\"Solution \"+solution+\" trial start\",\"\")});window.addEventListener(\"non_trial_form__submitted\",\nfunction(a){sendEventToGa(\"Price contact request\",a.detail.value.content)});break;case \/\\\/newsroom\\\/\/.test(window.location.pathname):sendEventToGa(\"Newsroom\"+pageviewSuffix,\"\"),window.addEventListener(\"trial_form__submitted\",function(){sendEventToGa(\"Newsroom trial start\",\"\")})};\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":151
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Evar aud=\/.*\\?.*targetID=[^\u0026]*aud-(\\d+).*\/g.exec(document.location.href);aud=null==aud?\"\":aud[1];ga\u0026\u0026\"\"!=aud\u0026\u0026ga(function(){if(ga.getAll()){var a=ga.getAll();if(a)for(var b=0;b\u003Ca.length;b++)trackerName=a[b].get(\"name\"),clientId=a[b].get(\"clientId\"),\"UA-43330364-7\"==a[b].get(\"trackingId\")\u0026\u0026(ga(trackerName+\".set\",{dimension12:aud}),ga(trackerName+\".send\",\"event\",\"service\",\"get_audience\",\"get_audience\",{nonInteraction:!0}))}});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":152
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Edocument.cookie=\"collaborate_2019_social\\x3d1; expires\\x3d Wed, 1 Jan 2020 00:00:00 PST\";\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":153
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"gaEvent\",eventCategory:\"Buttons\",eventAction:\"Collaborate register button click\",eventLabel:document.location.pathname});\"1\"==",["escape",["macro",42],8,16],"\u0026\u0026dataLayer.push({event:\"gaEvent\",eventCategory:\"Collaborate 2019\",eventAction:\"Social traffic reg link\",eventLabel:document.location.pathname});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":154
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Ewindow._6si=window._6si||[];window._6si.push([\"enableEventTracking\",!0]);window._6si.push([\"setToken\",\"651c7cbf0405118ba2ee8bd20d6e58e7\"]);window._6si.push([\"setEndpoint\",\"b.6sc.co\"]);(function(){var a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"\/\/j.6sc.co\/6si.min.js\";var b=document.getElementsByTagName(\"script\")[0];b.parentNode.insertBefore(a,b)})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":159
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction sendEventToGa(a,b){dataLayer.push({event:\"gaEvent\",eventCategory:\"Help_center\",eventAction:a,eventLabel:b})}window.addEventListener(\"faqItemInteraction\",function(a){sendEventToGa(a.detail.type,a.detail.name)});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":160
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efunction setCookie(c,d,b){var a=\"\";b\u0026\u0026(a=new Date,a.setTime(a.getTime()+864E5*b),a=\"; expires\\x3d\"+a.toUTCString());document.cookie=c+\"\\x3d\"+(d||\"\")+a+\"; domain\\x3d.wrike.com; path\\x3d\/;\"}function getCookie(c){c+=\"\\x3d\";for(var d=document.cookie.split(\";\"),b=0;b\u003Cd.length;b++){for(var a=d[b];\" \"===a.charAt(0);)a=a.substring(1,a.length);if(0===a.indexOf(c))return a.substring(c.length,a.length)}return null}\nfunction updateExperimentCookie(){var c={\".*eu_exp\\x3d1.*\":\"eu\",\".*eu_exp\\x3dremarketing.*\":\"eu\",\".*jp_exp\\x3d1.*\":\"jp\",\".*apac_exp\\x3d1.*\":\"apac\",\".*utm_source\\x3dg2crowd.*\":\"g2\",\".*utm_source\\x3dcapterra.*\":\"capterra\"},d;for(d in c){var b=new RegExp(d);if(b.test(document.location.href)){var a=c[d];break}}if(null==a)return null;b=getCookie(\"wrike_expid\");if(null!=b)for(d in c){var e=c[d];if(e===b)return null;if(e===a)break}setCookie(\"wrike_expid\",a,14);\"capterra\"===a\u0026\u0026setCookie(\"utm_source\",a,14)}\nupdateExperimentCookie();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":166
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar MathTag={version:\"1.1\",delimiter:\",\",previous_url:document.referrer,mt_exem:void 0,industry:\"Business Services\",mt_adid:\"234386\",event_type:\"catchall\",mt_id:\"1461122\",product_category:",["escape",["macro",10],8,16],",product_name:",["escape",["macro",9],8,16],",revenue:void 0};\u003C\/script\u003E\n\u003Cscript async type=\"text\/gtmscript\" data-gtmsrc=\"\/\/pixel.mathtag.com\/event\/js?mt_pp=1\u0026amp;mt_adid=234386\"\u003E\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":167
    },{
      "function":"__html",
      "metadata":["map"],
      "once_per_event":true,
      "vtp_html":["template","\n\u003Cscript type=\"text\/gtmscript\"\u003Evar MathTag={version:\"1.1\",delimiter:\",\",previous_url:document.referrer,mt_exem:void 0,industry:\"Business Services\",mt_adid:\"234386\",event_type:\"conversion\",mt_id:\"1461120\",product_category:",["escape",["macro",10],8,16],",product_name:",["escape",["macro",9],8,16],",revenue:void 0};\u003C\/script\u003E\n\u003Cscript async type=\"text\/gtmscript\" data-gtmsrc=\"\/\/pixel.mathtag.com\/event\/js?mt_pp=1\u0026amp;mt_adid=234386\"\u003E\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":168
    }],
  "predicates":[{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":".*"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gtm.js"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"technicalReload"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"www.wrike.com"
    },{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":"(analyticstracking|frontend)"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/blog\/"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"wrike.com\/help"
    },{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":"www.wrike.com\\\/library.*thank-you"
    },{
      "function":"_cn",
      "arg0":["macro",14],
      "arg1":"gaEvent"
    },{
      "function":"_re",
      "arg0":["macro",12],
      "arg1":".*"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/resend"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"sales"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"workspace"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"profile"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"accounts"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"analyticsframe"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"labs"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"analyticstracking"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"login"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/test-page-remarketing-1"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/wrike-savings"
    },{
      "function":"_eq",
      "arg0":["macro",4],
      "arg1":"corporate"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/affiliate-registration-success"
    },{
      "function":"_re",
      "arg0":["macro",12],
      "arg1":"(ebook|webinar|template)\\_form__submitted"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"pm_guide_form__submitted"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"emrr12"
    },{
      "function":"_cn",
      "arg0":["macro",17],
      "arg1":"Productivity Tour"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gaEvent"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"0.7"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"emrr"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"0.45"
    },{
      "function":"_sw",
      "arg0":["macro",21],
      "arg1":"RU"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"1.2"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"1"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"0.3"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"1.1"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"0.6"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"1.5"
    },{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":"(try|learn)\\.wrike\\.com",
      "ignore_case":true
    },{
      "function":"_eq",
      "arg0":["macro",7],
      "arg1":"1"
    },{
      "function":"_eq",
      "arg0":["macro",22],
      "arg1":"library.wrike.com"
    },{
      "function":"_eq",
      "arg0":["macro",23],
      "arg1":"75"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"techScroll"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gtm.timer"
    },{
      "function":"_re",
      "arg0":["macro",24],
      "arg1":"(^$|((^|,)215620_346($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"\/library\/ebooks\/.+thank-you\/"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"collaborate.wrike.com"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/apps"
    },{
      "function":"_cn",
      "arg0":["macro",14],
      "arg1":"gtm.linkClick"
    },{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":"\/apps\/.*",
      "ignore_case":true
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/library\/ebooks\/.*thank-you"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"try.wrike.com\/all-in-one-collaboration-project-management-software"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"^\/help\/"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/avoid-marketing-horror-stories"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/design-clients-from-hell"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/the-true-cost-of-doing-nothing"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/overcome-creative-productivity-roadblocks"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/reclaim-your-creative-time"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/best-marketing-management-software"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/marketing-project-management-software"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/become-agile-with-wrike"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/marketing-operations-management-software"
    },{
      "function":"_re",
      "arg0":["macro",11],
      "arg1":"go.wrike.com\/(agile-marketing-quiz|agile-quiz)"
    },{
      "function":"_sw",
      "arg0":["macro",11],
      "arg1":"\/project-management-guide"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/project-management\/"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/marketing-project-management\/"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/creative-project-management\/"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/blog\/"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/product-management-software"
    },{
      "function":"_sw",
      "arg0":["macro",9],
      "arg1":"\/business-operations-teams"
    },{
      "function":"_sw",
      "arg0":["macro",9],
      "arg1":"\/professional-services-management"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/team-collaboration-software\/"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"^\\\/(v[a-z]?\\\/)?$"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"cdn.wrike.com\/image\/lp-assets\/ny-campaign\/index.html"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"help.wrike.com"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/web-based-task-management-software"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/3-prehistoric-tools"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/is-wrike-right-for-you"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/all-in-one-collaboration-and-project-management"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/blogpost\/"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/wrike-at-martech\/"
    },{
      "function":"_cn",
      "arg0":["macro",32],
      "arg1":"\/wrike-at-martech\/clkn\/https\/calendly.com\/wrike-team\/15-minute-demo\/"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"learn.wrike.com\/productivity-tour"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"try.wrike.com\/g-suite"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/proofing-and-approval-add-on\/"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/easy-project-planning"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/main\/"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/before-after-pm"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"^\\\/([cmp]r\\\/)?$"
    },{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":"^\/(ru|fr|de|es|ja)\/$"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/gantt-chart"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"train-your-brain"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/halloween-2017\/index.html"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/self-assessment-survey"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"mark_as_read"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"remind.htm"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"frontend"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"open-wrike-app"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"reset-password-confirm"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"resend"
    },{
      "function":"_cn",
      "arg0":["macro",22],
      "arg1":"library.wrike.com"
    },{
      "function":"_re",
      "arg0":["macro",23],
      "arg1":"(25|50|75|100)"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gtm.linkClick"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/careers\/"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/davids-interview"
    },{
      "function":"_ge",
      "arg0":["macro",20],
      "arg1":"4"
    },{
      "function":"_sw",
      "arg0":["macro",21],
      "arg1":"US"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gtm.load"
    },{
      "function":"_cn",
      "arg0":["macro",9],
      "arg1":"\/frontend\/"
    },{
      "function":"_css",
      "arg0":["macro",37],
      "arg1":".share-popup .buttons-wrapper div"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gtm.click"
    },{
      "function":"_cn",
      "arg0":["macro",11],
      "arg1":"\/price"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"gtm.scrollDepth"
    },{
      "function":"_re",
      "arg0":["macro",24],
      "arg1":"(^$|((^|,)215620_347($|,)))"
    },{
      "function":"_eq",
      "arg0":["macro",41],
      "arg1":"Social"
    },{
      "function":"_re",
      "arg0":["macro",40],
      "arg1":"^(Collaborate_2019|Wrike_Collaborate|Wrike_Collaborate_Nashville|Wrike_Collaborate_London|Collaborate_Nashville|Collaborate_London)$"
    },{
      "function":"_cn",
      "arg0":["macro",32],
      "arg1":"www.cvent.com\/d\/"
    },{
      "function":"_re",
      "arg0":["macro",24],
      "arg1":"(^$|((^|,)215620_375($|,)))"
    },{
      "function":"_eq",
      "arg0":["macro",22],
      "arg1":"help.wrike.com"
    }],
  "rules":[
    [["if",0,1],["add",0,5,40,41,44,56]],
    [["if",2],["add",0,1,5,7,8,19,30,39,40,41,44,48,49,51,56,58,59,63,77,86,95,96,97,100,109,110]],
    [["if",1,3],["add",1]],
    [["if",1,5],["add",2,55,68,76]],
    [["if",1,6],["add",2,46]],
    [["if",1,7],["add",3]],
    [["if",8,9],["add",4,10]],
    [["if",1,10],["unless",11],["add",6,9,31,102,117]],
    [["if",1],["unless",12,13,14,15,16,17,18],["add",7,8,19,30,39,48,58,59,77,86,96,100,104,105,109,116]],
    [["if",1,20],["add",11,73]],
    [["if",1,21],["add",12,32,92]],
    [["if",1,22],["add",13]],
    [["if",23],["add",14,69,88,89]],
    [["if",24],["add",14]],
    [["if",10,25],["add",15]],
    [["if",26,27],["add",16]],
    [["if",10,28,29],["add",17]],
    [["if",10,29,30],["add",18]],
    [["if",10,29,32],["add",20]],
    [["if",10,29,33],["add",21]],
    [["if",10,29,34],["add",22]],
    [["if",10,29,35],["add",23]],
    [["if",10,29,36],["add",24]],
    [["if",10,29,37],["add",25]],
    [["if",1,38],["add",26,79]],
    [["if",23,39],["add",27]],
    [["if",40,41,42],["add",28]],
    [["if",43,44],["add",29]],
    [["if",1,45],["add",33,60]],
    [["if",1],["add",34,36,37,49,51,63,97,110,115]],
    [["if",1,40],["add",35]],
    [["if",1,46],["add",38]],
    [["if",1,47],["add",42]],
    [["if",9,48,49],["add",43]],
    [["if",1,50],["add",45]],
    [["if",1,51],["add",47,64]],
    [["if",1,52],["add",50]],
    [["if",1,53],["add",52,53,54]],
    [["if",1,54],["add",52,53]],
    [["if",1,55],["add",52]],
    [["if",1,56],["add",52]],
    [["if",1,57],["add",52]],
    [["if",1,58],["add",54]],
    [["if",1,59],["add",54]],
    [["if",1,60],["add",54]],
    [["if",1,61],["add",54]],
    [["if",1,62],["add",57]],
    [["if",1,63],["add",60]],
    [["if",1,64],["add",60]],
    [["if",1,65],["add",60]],
    [["if",1,66],["add",60]],
    [["if",1,67],["add",60]],
    [["if",1,68],["add",60]],
    [["if",1,69],["add",60]],
    [["if",1,70],["add",60]],
    [["if",1,71],["add",60]],
    [["if",1,72],["add",60,75,79]],
    [["if",1,73],["add",61]],
    [["if",1,74],["add",62,81,94]],
    [["if",1,75],["add",64]],
    [["if",1,76],["add",65]],
    [["if",1,77],["add",66]],
    [["if",1,78],["add",67]],
    [["if",1,79],["add",68,74]],
    [["if",9,48,80,81],["add",70]],
    [["if",1,82],["add",71]],
    [["if",1,83],["add",72]],
    [["if",1,84],["add",75]],
    [["if",1,85],["add",75,79]],
    [["if",1,86],["add",75,79]],
    [["if",1,87],["add",78]],
    [["if",1,88],["add",79]],
    [["if",1,89],["add",79]],
    [["if",1,90],["add",79]],
    [["if",1,91],["add",80]],
    [["if",1,92],["add",82]],
    [["if",1,93],["add",83]],
    [["if",42,101],["unless",14,17,94,95,96,97,98,99,100],["add",84]],
    [["if",102],["add",85,106,107,108]],
    [["if",1,103],["add",87]],
    [["if",1,104],["add",90]],
    [["if",10,29,105],["add",91]],
    [["if",106,107],["add",93]],
    [["if",107],["unless",12,13,14,15,16,17,18],["add",95,113]],
    [["if",40,42],["add",98]],
    [["if",109,110],["add",99]],
    [["if",1,111],["add",101]],
    [["if",112,113],["add",103]],
    [["if",1,46,114,115],["add",111]],
    [["if",102,116,117],["add",112]],
    [["if",1,118],["add",114]],
    [["if",1,4],["block",1,96]],
    [["if",1,19],["block",7]],
    [["if",1,31],["block",19,89]],
    [["if",107,108],["block",95]],
    [["if",1],["unless",40],["block",99]]]
},
"runtime":[]




};
/*

 Copyright The Closure Library Authors.
 SPDX-License-Identifier: Apache-2.0
*/
var aa,ba="function"==typeof Object.create?Object.create:function(a){var b=function(){};b.prototype=a;return new b},ca;if("function"==typeof Object.setPrototypeOf)ca=Object.setPrototypeOf;else{var da;a:{var ea={lf:!0},fa={};try{fa.__proto__=ea;da=fa.lf;break a}catch(a){}da=!1}ca=da?function(a,b){a.__proto__=b;if(a.__proto__!==b)throw new TypeError(a+" is not extensible");return a}:null}var ia=ca,ja=this||self,la=/^[\w+/_-]+[=]{0,2}$/,ma=null;var pa=function(){},qa=function(a){return"function"==typeof a},g=function(a){return"string"==typeof a},ra=function(a){return"number"==typeof a&&!isNaN(a)},ua=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},r=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},va=function(a,b){if(a&&ua(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},wa=function(a,b){if(!ra(a)||
!ra(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},ya=function(a,b){for(var c=new xa,d=0;d<a.length;d++)c.set(a[d],!0);for(var e=0;e<b.length;e++)if(c.get(b[e]))return!0;return!1},za=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},Aa=function(a){return Math.round(Number(a))||0},Ba=function(a){return"false"==String(a).toLowerCase()?!1:!!a},Ca=function(a){var b=[];if(ua(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},Ea=function(a){return a?
a.replace(/^\s+|\s+$/g,""):""},Fa=function(){return(new Date).getTime()},xa=function(){this.prefix="gtm.";this.values={}};xa.prototype.set=function(a,b){this.values[this.prefix+a]=b};xa.prototype.get=function(a){return this.values[this.prefix+a]};
var Ga=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},Ha=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},Ia=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},Ja=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Ka=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c},La=function(a,b){for(var c={},d=c,e=a.split("."),f=0;f<e.length-1;f++)d=d[e[f]]={};d[e[e.length-1]]=b;return c},Ma=function(a){var b=
[];za(a,function(c,d){10>c.length&&d&&b.push(c)});return b.join(",")},Na=function(a){for(var b=[],c=0;c<a.length;c++){var d=a.charCodeAt(c);128>d?b.push(d):2048>d?b.push(192|d>>6,128|d&63):55296>d||57344<=d?b.push(224|d>>12,128|d>>6&63,128|d&63):(d=65536+((d&1023)<<10|a.charCodeAt(++c)&1023),b.push(240|d>>18,128|d>>12&63,128|d>>6&63,128|d&63))}return new Uint8Array(b)};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Oa=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Pa=function(a){if(null==a)return String(a);var b=Oa.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Qa=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Ra=function(a){if(!a||"object"!=Pa(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Qa(a,"constructor")&&!Qa(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Qa(a,b)},B=function(a,b){var c=b||("array"==Pa(a)?[]:{}),d;for(d in a)if(Qa(a,d)){var e=a[d];"array"==Pa(e)?("array"!=Pa(c[d])&&(c[d]=[]),c[d]=B(e,c[d])):Ra(e)?(Ra(c[d])||(c[d]={}),c[d]=B(e,c[d])):c[d]=e}return c};
var Sa=[],Ta={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},Ua=function(a){return Ta[a]},Va=/[\x00\x22\x26\x27\x3c\x3e]/g;var Za=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,cb={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},db=function(a){return cb[a]};
Sa[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(Za,db)+"'"}};var jb=/['()]/g,lb=function(a){return"%"+a.charCodeAt(0).toString(16)};Sa[12]=function(a){var b=
encodeURIComponent(String(a));jb.lastIndex=0;return jb.test(b)?b.replace(jb,lb):b};var mb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,nb={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},ob=function(a){return nb[a]};Sa[16]=function(a){return a};var qb;
var rb=[],sb=[],tb=[],vb=[],wb=[],xb={},yb,zb,Ab,Bb=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},Cb=function(a,b){var c=a["function"];if(!c)throw Error("Error: No function name given for function call.");var d=xb[c],e={},f;for(f in a)a.hasOwnProperty(f)&&0===f.indexOf("vtp_")&&(e[void 0!==d?f:f.substr(4)]=a[f]);return void 0!==d?d(e):qb(c,e,b)},Eb=function(a,b,c){c=c||[];var d={},e;for(e in a)a.hasOwnProperty(e)&&(d[e]=Db(a[e],b,c));
return d},Fb=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=xb[b];return c?c.priorityOverride||0:0},Db=function(a,b,c){if(ua(a)){var d;switch(a[0]){case "function_id":return a[1];case "list":d=[];for(var e=1;e<a.length;e++)d.push(Db(a[e],b,c));return d;case "macro":var f=a[1];if(c[f])return;var h=rb[f];if(!h||b.Lc(h))return;c[f]=!0;try{var k=Eb(h,b,c);k.vtp_gtmEventId=b.id;d=Cb(k,b);Ab&&(d=Ab.Mf(d,k))}catch(y){b.te&&b.te(y,Number(f)),d=!1}c[f]=
!1;return d;case "map":d={};for(var l=1;l<a.length;l+=2)d[Db(a[l],b,c)]=Db(a[l+1],b,c);return d;case "template":d=[];for(var m=!1,n=1;n<a.length;n++){var q=Db(a[n],b,c);zb&&(m=m||q===zb.ub);d.push(q)}return zb&&m?zb.Pf(d):d.join("");case "escape":d=Db(a[1],b,c);if(zb&&ua(a[1])&&"macro"===a[1][0]&&zb.mg(a))return zb.Jg(d);d=String(d);for(var u=2;u<a.length;u++)Sa[a[u]]&&(d=Sa[a[u]](d));return d;case "tag":var p=a[1];if(!vb[p])throw Error("Unable to resolve tag reference "+p+".");return d={fe:a[2],
index:p};case "zb":var t={arg0:a[2],arg1:a[3],ignore_case:a[5]};t["function"]=a[1];var v=Gb(t,b,c),w=!!a[4];return w||2!==v?w!==(1===v):null;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},Gb=function(a,b,c){try{return yb(Eb(a,b,c))}catch(d){JSON.stringify(a)}return 2};var Ib=function(){var a=function(b){return{toString:function(){return b}}};return{qd:a("convert_case_to"),rd:a("convert_false_to"),sd:a("convert_null_to"),td:a("convert_true_to"),ud:a("convert_undefined_to"),rh:a("debug_mode_metadata"),ra:a("function"),Qe:a("instance_name"),Ue:a("live_only"),We:a("malware_disabled"),Xe:a("metadata"),sh:a("original_vendor_template_id"),af:a("once_per_event"),Dd:a("once_per_load"),Ld:a("setup_tags"),Nd:a("tag_id"),Od:a("teardown_tags")}}();var Jb=null,Mb=function(a){function b(q){for(var u=0;u<q.length;u++)d[q[u]]=!0}var c=[],d=[];Jb=Kb(a);for(var e=0;e<sb.length;e++){var f=sb[e],h=Lb(f);if(h){for(var k=f.add||[],l=0;l<k.length;l++)c[k[l]]=!0;b(f.block||[])}else null===h&&b(f.block||[])}for(var m=[],n=0;n<vb.length;n++)c[n]&&!d[n]&&(m[n]=!0);return m},Lb=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=Jb(b[c]);if(0===d)return!1;if(2===d)return null}for(var e=a.unless||[],f=0;f<e.length;f++){var h=Jb(e[f]);if(2===h)return null;
if(1===h)return!1}return!0},Kb=function(a){var b=[];return function(c){void 0===b[c]&&(b[c]=Gb(tb[c],a));return b[c]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */
var D=window,F=document,fc=navigator,gc=F.currentScript&&F.currentScript.src,hc=function(a,b){var c=D[a];D[a]=void 0===c?b:c;return D[a]},ic=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},jc=function(a,b,c){var d=F.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;ic(d,b);c&&(d.onerror=c);var e;if(null===ma)b:{var f=ja.document,h=f.querySelector&&f.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&la.test(k)){ma=k;break b}}ma=""}e=ma;e&&d.setAttribute("nonce",e);var l=F.getElementsByTagName("script")[0]||F.body||F.head;l.parentNode.insertBefore(d,l);return d},kc=function(){if(gc){var a=gc.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},lc=function(a,b){var c=F.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=F.body&&F.body.lastChild||
F.body||F.head;d.parentNode.insertBefore(c,d);ic(c,b);void 0!==a&&(c.src=a);return c},mc=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},nc=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},oc=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},G=function(a){D.setTimeout(a,0)},qc=function(a,b){return a&&
b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},rc=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},sc=function(a){var b=F.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},tc=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var f=a,h=0;f&&h<=c;h++){if(d[String(f.tagName).toLowerCase()])return f;
f=f.parentElement}return null},uc=function(a,b){var c=a[b];c&&"string"===typeof c.animVal&&(c=c.animVal);return c};var wc=function(a){return vc?F.querySelectorAll(a):null},xc=function(a,b){if(!vc)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!F.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},yc=!1;if(F.querySelectorAll)try{var zc=F.querySelectorAll(":root");zc&&1==zc.length&&zc[0]==F.documentElement&&(yc=!0)}catch(a){}var vc=yc;var H={qa:"_ee",nc:"event_callback",tb:"event_timeout",D:"gtag.config",X:"allow_ad_personalization_signals",oc:"restricted_data_processing",Qa:"allow_google_signals",Y:"cookie_expires",sb:"cookie_update",Ra:"session_duration",ca:"user_properties"};
H.de=[H.X,H.Qa,H.sb];H.ke=[H.Y,H.tb,H.Ra];var Pc=/[A-Z]+/,Qc=/\s/,Rc=function(a){if(g(a)&&(a=Ea(a),!Qc.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(Pc.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],o:d}}}}},Tc=function(a){for(var b={},c=0;c<a.length;++c){var d=Rc(a[c]);d&&(b[d.id]=d)}Sc(b);var e=[];za(b,function(f,h){e.push(h)});return e};
function Sc(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.o[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var Uc={},Vc=null,Wc=Math.random();Uc.s="GTM-WRBD5L";Uc.yb="1t0";var Xc={__cl:!0,__ecl:!0,__ehl:!0,__evl:!0,__fal:!0,__fil:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0,__paused:!0,__tg:!0},Yc="www.googletagmanager.com/gtm.js";var Zc=Yc,$c=null,bd=null,cd=null,dd="//www.googletagmanager.com/a?id="+Uc.s+"&cv=493",ed={},fd={},gd=function(){var a=Vc.sequence||0;Vc.sequence=a+1;return a};var hd={},I=function(a,b){hd[a]=hd[a]||[];hd[a][b]=!0},id=function(a){for(var b=[],c=hd[a]||[],d=0;d<c.length;d++)c[d]&&(b[Math.floor(d/6)]^=1<<d%6);for(var e=0;e<b.length;e++)b[e]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(b[e]||0);return b.join("")};
var jd=function(){return"&tc="+vb.filter(function(a){return a}).length},md=function(){kd||(kd=D.setTimeout(ld,500))},ld=function(){kd&&(D.clearTimeout(kd),kd=void 0);void 0===nd||od[nd]&&!pd&&!qd||(rd[nd]||sd.og()||0>=td--?(I("GTM",1),rd[nd]=!0):(sd.Sg(),mc(ud()),od[nd]=!0,vd=wd=qd=pd=""))},ud=function(){var a=nd;if(void 0===a)return"";var b=id("GTM"),c=id("TAGGING");return[xd,od[a]?"":"&es=1",yd[a],b?"&u="+b:"",c?"&ut="+c:"",jd(),pd,qd,wd,vd,"&z=0"].join("")},zd=function(){return[dd,"&v=3&t=t","&pid="+
wa(),"&rv="+Uc.yb].join("")},Ad="0.005000">Math.random(),xd=zd(),Bd=function(){xd=zd()},od={},pd="",qd="",vd="",wd="",nd=void 0,yd={},rd={},kd=void 0,sd=function(a,b){var c=0,d=0;return{og:function(){if(c<a)return!1;Fa()-d>=b&&(c=0);return c>=a},Sg:function(){Fa()-d>=b&&(c=0);c++;d=Fa()}}}(2,1E3),td=1E3,Cd=function(a,b){if(Ad&&!rd[a]&&nd!==a){ld();nd=a;vd=pd="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):"*";yd[a]="&e="+c+"&eid="+a;md()}},Dd=function(a,b,c){if(Ad&&!rd[a]&&
b){a!==nd&&(ld(),nd=a);var d,e=String(b[Ib.ra]||"").replace(/_/g,"");0===e.indexOf("cvt")&&(e="cvt");d=e;var f=c+d;pd=pd?pd+"."+f:"&tr="+f;var h=b["function"];if(!h)throw Error("Error: No function name given for function call.");var k=(xb[h]?"1":"2")+d;vd=vd?vd+"."+k:"&ti="+k;md();2022<=ud().length&&ld()}},Ed=function(a,b,c){if(Ad&&!rd[a]){a!==nd&&(ld(),nd=a);var d=c+b;qd=qd?qd+
"."+d:"&epr="+d;md();2022<=ud().length&&ld()}};var Fd={},Gd=new xa,Hd={},Id={},Ld={name:"dataLayer",set:function(a,b){B(La(a,b),Hd);Jd()},get:function(a){return Kd(a,2)},reset:function(){Gd=new xa;Hd={};Jd()}},Kd=function(a,b){if(2!=b){var c=Gd.get(a);if(Ad){var d=Md(a);c!==d&&I("GTM",5)}return c}return Md(a)},Md=function(a,b,c){var d=a.split("."),e=!1,f=void 0;return e?f:Od(d)},Od=function(a){for(var b=Hd,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var Qd=function(a,b){Id.hasOwnProperty(a)||(Gd.set(a,b),B(La(a,b),Hd),Jd())},Jd=function(a){za(Id,function(b,c){Gd.set(b,c);B(La(b,void 0),Hd);B(La(b,c),Hd);a&&delete Id[b]})},Rd=function(a,b,c){Fd[a]=Fd[a]||{};var d=1!==c?Md(b):Gd.get(b);"array"===Pa(d)||"object"===Pa(d)?Fd[a][b]=B(d):Fd[a][b]=d},Sd=function(a,b){if(Fd[a])return Fd[a][b]},Td=function(a,b){Fd[a]&&delete Fd[a][b]};var Ud=function(){var a=!1;return a};var Q=function(a,b,c,d){return(2===Vd()||d||"http:"!=D.location.protocol?a:b)+c},Vd=function(){var a=kc(),b;if(1===a)a:{var c=Zc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,f=1,h=F.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===f&&0===l.indexOf(d)&&(f=2)}}b=f}else b=a;return b};var je=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),ke={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},le={cl:["ecl"],customPixels:["customScripts","html"],
ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts"],customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]},me="google customPixels customScripts html nonGooglePixels nonGoogleScripts nonGoogleIframes".split(" ");
var oe=function(a){var b=Kd("gtm.whitelist");b&&I("GTM",9);var c=b&&Ka(Ca(b),ke),d=Kd("gtm.blacklist");d||(d=Kd("tagTypeBlacklist"))&&I("GTM",3);d?
I("GTM",8):d=[];ne()&&(d=Ca(d),d.push("nonGooglePixels","nonGoogleScripts","sandboxedScripts"));0<=r(Ca(d),"google")&&I("GTM",2);var e=d&&Ka(Ca(d),le),f={};return function(h){var k=h&&h[Ib.ra];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==f[k])return f[k];var l=fd[k]||[],m=a(k,l);if(b){var n;if(n=m)a:{if(0>r(c,k))if(l&&0<l.length)for(var q=0;q<
l.length;q++){if(0>r(c,l[q])){I("GTM",11);n=!1;break a}}else{n=!1;break a}n=!0}m=n}var u=!1;if(d){var p=0<=r(e,k);if(p)u=p;else{var t=ya(e,l||[]);t&&I("GTM",10);u=t}}var v=!m||u;v||!(0<=r(l,"sandboxedScripts"))||c&&-1!==r(c,"sandboxedScripts")||(v=ya(e,me));return f[k]=v}},ne=function(){return je.test(D.location&&D.location.hostname)};var pe={Mf:function(a,b){b[Ib.qd]&&"string"===typeof a&&(a=1==b[Ib.qd]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(Ib.sd)&&null===a&&(a=b[Ib.sd]);b.hasOwnProperty(Ib.ud)&&void 0===a&&(a=b[Ib.ud]);b.hasOwnProperty(Ib.td)&&!0===a&&(a=b[Ib.td]);b.hasOwnProperty(Ib.rd)&&!1===a&&(a=b[Ib.rd]);return a}};var qe={active:!0,isWhitelisted:function(){return!0}},re=function(a){var b=Vc.zones;!b&&a&&(b=Vc.zones=a());return b};var se=function(){};var te=!1,ue=0,ve=[];function we(a){if(!te){var b=F.createEventObject,c="complete"==F.readyState,d="interactive"==F.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){te=!0;for(var e=0;e<ve.length;e++)G(ve[e])}ve.push=function(){for(var f=0;f<arguments.length;f++)G(arguments[f]);return 0}}}function xe(){if(!te&&140>ue){ue++;try{F.documentElement.doScroll("left"),we()}catch(a){D.setTimeout(xe,50)}}}var ye=function(a){te?a():ve.push(a)};var ze={},Ae={},Be=function(a,b,c,d){if(!Ae[a]||Xc[b]||"__zone"===b)return-1;var e={};Ra(d)&&(e=B(d,e));e.id=c;e.status="timeout";return Ae[a].tags.push(e)-1},Ce=function(a,b,c,d){if(Ae[a]){var e=Ae[a].tags[b];e&&(e.status=c,e.executionTime=d)}};function De(a){for(var b=ze[a]||[],c=0;c<b.length;c++)b[c]();ze[a]={push:function(d){d(Uc.s,Ae[a])}}}
var Ge=function(a,b,c){Ae[a]={tags:[]};qa(b)&&Ee(a,b);c&&D.setTimeout(function(){return De(a)},Number(c));return Fe(a)},Ee=function(a,b){ze[a]=ze[a]||[];ze[a].push(Ha(function(){return G(function(){b(Uc.s,Ae[a])})}))};function Fe(a){var b=0,c=0,d=!1;return{add:function(){c++;return Ha(function(){b++;d&&b>=c&&De(a)})},yf:function(){d=!0;b>=c&&De(a)}}};var He=function(){function a(d){return!ra(d)||0>d?0:d}if(!Vc._li&&D.performance&&D.performance.timing){var b=D.performance.timing.navigationStart,c=ra(Ld.get("gtm.start"))?Ld.get("gtm.start"):0;Vc._li={cst:a(c-b),cbt:a(bd-b)}}};var Le={},Me=function(){return D.GoogleAnalyticsObject&&D[D.GoogleAnalyticsObject]},Ne=!1;
var Oe=function(a){D.GoogleAnalyticsObject||(D.GoogleAnalyticsObject=a||"ga");var b=D.GoogleAnalyticsObject;if(D[b])D.hasOwnProperty(b)||I("GTM",12);else{var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);D[b]=c}He();return D[b]},Pe=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=Me();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var Re=function(a){},Qe=function(){return D.GoogleAnalyticsObject||"ga"};var Te=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var Ue=/:[0-9]+$/,Ve=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var f=d[e].split("=");if(decodeURIComponent(f[0]).replace(/\+/g," ")===b){var h=f.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},Ye=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=We(a.protocol)||We(D.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:D.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||D.location.hostname).replace(Ue,"").toLowerCase());var f=b,h,k=We(a.protocol);f&&(f=String(f).toLowerCase());switch(f){case "url_no_fragment":h=Xe(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace(Ue,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":a.pathname||a.hostname||I("TAGGING",1);h="/"==a.pathname.substr(0,1)?a.pathname:
"/"+a.pathname;var m=h.split("/");0<=r(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=Ve(h,e,void 0));break;case "extension":var n=a.pathname.split(".");h=1<n.length?n[n.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},We=function(a){return a?a.replace(":","").toLowerCase():""},Xe=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},
Ze=function(a){var b=F.createElement("a");a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(a||I("TAGGING",1),c="/"+c);var d=b.hostname.replace(Ue,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};function df(a,b,c,d){var e=vb[a],f=ef(a,b,c,d);if(!f)return null;var h=Db(e[Ib.Ld],c,[]);if(h&&h.length){var k=h[0];f=df(k.index,{B:f,w:1===k.fe?b.terminate:f,terminate:b.terminate},c,d)}return f}
function ef(a,b,c,d){function e(){if(f[Ib.We])k();else{var w=Eb(f,c,[]),y=Be(c.id,String(f[Ib.ra]),Number(f[Ib.Nd]),w[Ib.Xe]),x=!1;w.vtp_gtmOnSuccess=function(){if(!x){x=!0;var A=Fa()-C;Dd(c.id,vb[a],"5");Ce(c.id,y,"success",A);h()}};w.vtp_gtmOnFailure=function(){if(!x){x=!0;var A=Fa()-C;Dd(c.id,vb[a],"6");Ce(c.id,y,"failure",A);k()}};w.vtp_gtmTagId=f.tag_id;
w.vtp_gtmEventId=c.id;Dd(c.id,f,"1");var z=function(){var A=Fa()-C;Dd(c.id,f,"7");Ce(c.id,y,"exception",A);x||(x=!0,k())};var C=Fa();try{Cb(w,c)}catch(A){z(A)}}}var f=vb[a],h=b.B,k=b.w,l=b.terminate;if(c.Lc(f))return null;var m=Db(f[Ib.Od],c,[]);if(m&&m.length){var n=m[0],q=df(n.index,{B:h,w:k,terminate:l},c,d);if(!q)return null;h=q;k=2===n.fe?l:q}if(f[Ib.Dd]||f[Ib.af]){var u=f[Ib.Dd]?wb:c.ah,p=h,t=k;if(!u[a]){e=Ha(e);var v=ff(a,u,e);h=v.B;k=v.w}return function(){u[a](p,t)}}return e}
function ff(a,b,c){var d=[],e=[];b[a]=gf(d,e,c);return{B:function(){b[a]=hf;for(var f=0;f<d.length;f++)d[f]()},w:function(){b[a]=jf;for(var f=0;f<e.length;f++)e[f]()}}}function gf(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function hf(a){a()}function jf(a,b){b()};var mf=function(a,b){for(var c=[],d=0;d<vb.length;d++)if(a.kb[d]){var e=vb[d];var f=b.add();try{var h=df(d,{B:f,w:f,terminate:f},a,d);h?c.push({Ee:d,ze:Fb(e),Xf:h}):(kf(d,a),f())}catch(l){f()}}b.yf();c.sort(lf);for(var k=0;k<c.length;k++)c[k].Xf();return 0<c.length};function lf(a,b){var c,d=b.ze,e=a.ze;c=d>e?1:d<e?-1:0;var f;if(0!==c)f=c;else{var h=a.Ee,k=b.Ee;f=h>k?1:h<k?-1:0}return f}
function kf(a,b){if(!Ad)return;var c=function(d){var e=b.Lc(vb[d])?"3":"4",f=Db(vb[d][Ib.Ld],b,[]);f&&f.length&&c(f[0].index);Dd(b.id,vb[d],e);var h=Db(vb[d][Ib.Od],b,[]);h&&h.length&&c(h[0].index)};c(a);}
var nf=!1,of=function(a,b,c,d,e){if("gtm.js"==b){if(nf)return!1;nf=!0}Cd(a,b);var f=Ge(a,d,e);Rd(a,"event",1);Rd(a,"ecommerce",1);Rd(a,"gtm");var h={id:a,name:b,Lc:oe(c),kb:[],ah:[],te:function(){I("GTM",6)}};h.kb=Mb(h);var k=mf(h,f);"gtm.js"!==b&&"gtm.sync"!==b||Re(Uc.s);if(!k)return k;for(var l=0;l<h.kb.length;l++)if(h.kb[l]){var m=vb[l];if(m&&!Xc[String(m[Ib.ra])])return!0}return!1};var qf=/^https?:\/\/www\.googletagmanager\.com/;function rf(){var a;return a}function tf(a,b){}
function sf(a){0!==a.indexOf("http://")&&0!==a.indexOf("https://")&&(a="https://"+a);"/"===a[a.length-1]&&(a=a.substring(0,a.length-1));return a}function uf(){var a=!1;return a};var vf=function(){this.eventModel={};this.targetConfig={};this.containerConfig={};this.h={};this.globalConfig={};this.B=function(){};this.w=function(){}},wf=function(a){var b=new vf;b.eventModel=a;return b},xf=function(a,b){a.targetConfig=b;return a},yf=function(a,b){a.containerConfig=b;return a},zf=function(a,b){a.h=b;return a},Af=function(a,b){a.globalConfig=b;return a},Bf=function(a,b){a.B=b;return a},Cf=function(a,b){a.w=b;return a};
vf.prototype.getWithConfig=function(a){if(void 0!==this.eventModel[a])return this.eventModel[a];if(void 0!==this.targetConfig[a])return this.targetConfig[a];if(void 0!==this.containerConfig[a])return this.containerConfig[a];if(void 0!==this.h[a])return this.h[a];if(void 0!==this.globalConfig[a])return this.globalConfig[a]};
var Df=function(a){function b(e){za(e,function(f){c[f]=null})}var c={};b(a.eventModel);b(a.targetConfig);b(a.containerConfig);b(a.globalConfig);var d=[];za(c,function(e){d.push(e)});return d};var Ef={},Ff=["G"];Ef.Fe="";var Gf=Ef.Fe.split(",");function Hf(){var a=Vc;return a.gcq=a.gcq||new If}
var Jf=function(a,b,c){Hf().register(a,b,c)},Kf=function(a,b,c,d){Hf().push("event",[b,a],c,d)},Lf=function(a,b){Hf().push("config",[a],b)},Mf={},Nf=function(){this.status=1;this.containerConfig={};this.targetConfig={};this.i={};this.m=null;this.h=!1},Of=function(a,b,c,d,e){this.type=a;this.m=b;this.N=c||"";this.h=d;this.i=e},If=function(){this.i={};this.m={};this.h=[]},Pf=function(a,b){var c=Rc(b);return a.i[c.containerId]=a.i[c.containerId]||new Nf},Qf=function(a,b,c,d){if(d.N){var e=Pf(a,d.N),
f=e.m;if(f){var h=B(c),k=B(e.targetConfig[d.N]),l=B(e.containerConfig),m=B(e.i),n=B(a.m),q=Kd("gtm.uniqueEventId"),u=Rc(d.N).prefix,p=Cf(Bf(Af(zf(yf(xf(wf(h),k),l),m),n),function(){Ed(q,u,"2");}),function(){Ed(q,u,"3");});try{Ed(q,u,"1");f(d.N,b,d.m,p)}catch(t){
Ed(q,u,"4");}}}};
If.prototype.register=function(a,b,c){if(3!==Pf(this,a).status){Pf(this,a).m=b;Pf(this,a).status=3;c&&(Pf(this,a).i=c);var d=Rc(a),e=Mf[d.containerId];if(void 0!==e){var f=Vc[d.containerId].bootstrap,h=d.prefix.toUpperCase();Vc[d.containerId]._spx&&(h=h.toLowerCase());var k=Kd("gtm.uniqueEventId"),l=h,m=Fa()-f;if(Ad&&!rd[k]){k!==nd&&(ld(),nd=k);var n=l+"."+Math.floor(f-e)+"."+Math.floor(m);wd=wd?wd+","+n:"&cl="+n}delete Mf[d.containerId]}this.flush()}};
If.prototype.push=function(a,b,c,d){var e=Math.floor(Fa()/1E3);if(c){var f=Rc(c),h;if(h=f){var k;if(k=1===Pf(this,c).status)a:{var l=f.prefix;k=!0}h=k}if(h&&(Pf(this,c).status=2,this.push("require",[],f.containerId),Mf[f.containerId]=Fa(),!Ud())){var m=encodeURIComponent(f.containerId),n=("http:"!=D.location.protocol?"https:":"http:")+
"//www.googletagmanager.com";jc(n+"/gtag/js?id="+m+"&l=dataLayer&cx=c")}}this.h.push(new Of(a,e,c,b,d));d||this.flush()};
If.prototype.flush=function(a){for(var b=this;this.h.length;){var c=this.h[0];if(c.i)c.i=!1,this.h.push(c);else switch(c.type){case "require":if(3!==Pf(this,c.N).status&&!a)return;break;case "set":za(c.h[0],function(l,m){B(La(l,m),b.m)});break;case "config":var d=c.h[0],e=!!d[H.Qb];delete d[H.Qb];var f=Pf(this,c.N),h=Rc(c.N),k=h.containerId===h.id;e||(k?f.containerConfig={}:f.targetConfig[c.N]={});f.h&&e||Qf(this,H.D,d,c);f.h=!0;delete d[H.qa];k?B(d,f.containerConfig):B(d,f.targetConfig[c.N]);break;
case "event":Qf(this,c.h[1],c.h[0],c)}this.h.shift()}};var Rf=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),f=0;f<e.length;f++){var h=e[f].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},Uf=function(a,b,c,d){var e=Sf(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=Tf(e,function(f){return f.Jb},b);if(1===e.length)return e[0].id;e=Tf(e,function(f){return f.lb},c);return e[0]?e[0].id:void 0}};
function Wf(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=Rf(b,e).indexOf(c)}
var Zf=function(a,b,c,d,e,f){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{f&&(l=encodeURIComponent(l));var n=l;n&&1200<n.length&&(n=n.substring(0,1200));l=n;m=a+"="+l}var q=void 0,u=void 0,p;for(p in h)if(h.hasOwnProperty(p)){var t=h[p];if(null!=t)switch(p){case "secure":t&&(m+="; secure");break;case "domain":q=t;break;default:"path"==p&&(u=t),"expires"==p&&t instanceof Date&&(t=
t.toUTCString()),m+="; "+p+"="+t}}if("auto"===q){for(var v=Xf(),w=0;w<v.length;++w){var y="none"!=v[w]?v[w]:void 0;if(!Yf(y,u)&&Wf(m+(y?"; domain="+y:""),a,l)){k=!0;break a}}k=!1}else q&&"none"!=q&&(m+="; domain="+q),k=!Yf(q,u)&&Wf(m,a,l)}return k};function Tf(a,b,c){for(var d=[],e=[],f,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===f||l<f?(e=[k],f=l):l===f&&e.push(k)}return 0<d.length?d:e}
function Sf(a,b){for(var c=[],d=Rf(a),e=0;e<d.length;e++){var f=d[e].split("."),h=f.shift();if(!b||-1!==b.indexOf(h)){var k=f.shift();k&&(k=k.split("-"),c.push({id:f.join("."),Jb:1*k[0]||1,lb:1*k[1]||1}))}}return c}
var $f=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,ag=/(^|\.)doubleclick\.net$/i,Yf=function(a,b){return ag.test(document.location.hostname)||"/"===b&&$f.test(a)},Xf=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));var e=document.location.hostname;ag.test(e)||$f.test(e)||a.push("none");return a};var bg="G".split(/,/),cg=!1;var dg=null,eg={},fg={},gg;function hg(a,b){var c={event:a};b&&(c.eventModel=B(b),b[H.nc]&&(c.eventCallback=b[H.nc]),b[H.tb]&&(c.eventTimeout=b[H.tb]));return c}
var ng={config:function(a){},
event:function(a){var b=a[1];if(g(b)&&!(3<a.length)){var c;if(2<a.length){if(!Ra(a[2])&&void 0!=a[2])return;c=a[2]}var d=hg(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(){},set:function(a){var b;2==a.length&&Ra(a[1])?b=B(a[1]):3==a.length&&g(a[1])&&(b={},Ra(a[2])||ua(a[2])?b[a[1]]=B(a[2]):b[a[1]]=a[2]);
if(b){b._clear=!0;return b}}},og={policy:!0};var pg=function(a,b){var c=a.hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}},rg=function(a){var b=qg(),c=b&&b.hide;c&&c.end&&(c[a]=!0)};var sg=!1,tg=[];function ug(){if(!sg){sg=!0;for(var a=0;a<tg.length;a++)G(tg[a])}}var vg=function(a){sg?G(a):tg.push(a)};var Kg=function(a){if(Jg(a))return a;this.h=a};Kg.prototype.dg=function(){return this.h};var Jg=function(a){return!a||"object"!==Pa(a)||Ra(a)?!1:"getUntrustedUpdateValue"in a};Kg.prototype.getUntrustedUpdateValue=Kg.prototype.dg;var Lg=[],Mg=!1,Ng=function(a){return D["dataLayer"].push(a)},Og=function(a){var b=Vc["dataLayer"],c=b?b.subscribers:1,d=0;return function(){++d===c&&a()}};
function Pg(a){var b=a._clear;za(a,function(f,h){"_clear"!==f&&(b&&Qd(f,void 0),Qd(f,h))});$c||($c=a["gtm.start"]);var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=gd(),a["gtm.uniqueEventId"]=d,Qd("gtm.uniqueEventId",d));cd=c;var e=
Qg(a);cd=null;switch(c){case "gtm.init":I("GTM",19),e&&I("GTM",20)}return e}function Qg(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=Vc.zones;d=e?e.checkState(Uc.s,c):qe;return d.active?of(c,b,d.isWhitelisted,a.eventCallback,a.eventTimeout)?!0:!1:!1}
function Rg(){for(var a=!1;!Mg&&0<Lg.length;){Mg=!0;delete Hd.eventModel;Jd();var b=Lg.shift();if(null!=b){var c=Jg(b);if(c){var d=b;b=Jg(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],f=0;f<e.length;f++){var h=e[f],k=Kd(h,1);if(ua(k)||Ra(k))k=B(k);Id[h]=k}}try{if(qa(b))try{b.call(Ld)}catch(v){}else if(ua(b)){var l=b;if(g(l[0])){var m=
l[0].split("."),n=m.pop(),q=l.slice(1),u=Kd(m.join("."),2);if(void 0!==u&&null!==u)try{u[n].apply(u,q)}catch(v){}}}else{var p=b;if(p&&("[object Arguments]"==Object.prototype.toString.call(p)||Object.prototype.hasOwnProperty.call(p,"callee"))){a:{if(b.length&&g(b[0])){var t=ng[b[0]];if(t&&(!c||!og[b[0]])){b=t(b);break a}}b=void 0}if(!b){Mg=!1;continue}}a=Pg(b)||a}}finally{c&&Jd(!0)}}Mg=!1}
return!a}function Sg(){var a=Rg();try{pg(D["dataLayer"],Uc.s)}catch(b){}return a}
var Ug=function(){var a=hc("dataLayer",[]),b=hc("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};ye(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});vg(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});b.subscribers=(b.subscribers||0)+1;var c=a.push;a.push=function(){var d;if(0<Vc.SANDBOXED_JS_SEMAPHORE){d=[];for(var e=0;e<arguments.length;e++)d[e]=new Kg(arguments[e])}else d=[].slice.call(arguments,0);var f=c.apply(a,d);Lg.push.apply(Lg,d);if(300<
this.length)for(I("GTM",4);300<this.length;)this.shift();var h="boolean"!==typeof f||f;return Rg()&&h};Lg.push.apply(Lg,a.slice(0));Tg()&&G(Sg)},Tg=function(){var a=!0;return a};var Vg={};Vg.ub=new String("undefined");
var Wg=function(a){this.h=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===Vg.ub?b:a[d]);return c.join("")}};Wg.prototype.toString=function(){return this.h("undefined")};Wg.prototype.valueOf=Wg.prototype.toString;Vg.jf=Wg;Vg.xc={};Vg.Pf=function(a){return new Wg(a)};var Xg={};Vg.Tg=function(a,b){var c=gd();Xg[c]=[a,b];return c};Vg.be=function(a){var b=a?0:1;return function(c){var d=Xg[c];if(d&&"function"===typeof d[b])d[b]();Xg[c]=void 0}};Vg.mg=function(a){for(var b=!1,c=!1,d=2;d<a.length;d++)b=
b||8===a[d],c=c||16===a[d];return b&&c};Vg.Jg=function(a){if(a===Vg.ub)return a;var b=gd();Vg.xc[b]=a;return'google_tag_manager["'+Uc.s+'"].macro('+b+")"};Vg.xg=function(a,b,c){a instanceof Vg.jf&&(a=a.h(Vg.Tg(b,c)),b=pa);return{Jc:a,B:b}};var Yg=function(a,b,c){function d(f,h){var k=f[h];return k}var e={event:b,"gtm.element":a,"gtm.elementClasses":d(a,"className"),"gtm.elementId":a["for"]||qc(a,"id")||"","gtm.elementTarget":a.formTarget||d(a,"target")||""};c&&(e["gtm.triggers"]=c.join(","));e["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||d(a,"href")||a.src||a.code||a.codebase||
"";return e},Zg=function(a){Vc.hasOwnProperty("autoEventsSettings")||(Vc.autoEventsSettings={});var b=Vc.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},$g=function(a,b,c){Zg(a)[b]=c},ah=function(a,b,c,d){var e=Zg(a),f=Ga(e,b,d);e[b]=c(f)},bh=function(a,b,c){var d=Zg(a);return Ga(d,b,c)};var ch=function(){for(var a=fc.userAgent+(F.cookie||"")+(F.referrer||""),b=a.length,c=D.history.length;0<c;)a+=c--^b++;var d=1,e,f,h;if(a)for(d=0,f=a.length-1;0<=f;f--)h=a.charCodeAt(f),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(Fa()/1E3)].join(".")},fh=function(a,b,c,d){var e=dh(b);return Uf(a,e,eh(c),d)},gh=function(a,b,c,d){var e=""+dh(c),f=eh(d);1<f&&(e+="-"+f);return[b,e,a].join(".")},dh=function(a){if(!a)return 1;
a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},eh=function(a){if(!a||"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};var hh=["1"],ih={},mh=function(a,b,c,d){var e=jh(a);ih[e]||kh(e,b,c)||(lh(e,ch(),b,c,d),kh(e,b,c))};function lh(a,b,c,d,e){var f=gh(b,"1",d,c);Zf(a,f,c,d,0==e?void 0:new Date(Fa()+1E3*(void 0==e?7776E3:e)))}function kh(a,b,c){var d=fh(a,b,c,hh);d&&(ih[a]=d);return d}function jh(a){return(a||"_gcl")+"_au"};var nh=function(){for(var a=[],b=F.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({dd:e[1],value:e[2]})}var f={};if(!a||!a.length)return f;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(f[a[h].dd]||(f[a[h].dd]=[]),f[a[h].dd].push({timestamp:k[1],$f:k[2]}))}return f};function oh(){for(var a=ph,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function qh(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}var ph,rh;function sh(a){ph=ph||qh();rh=rh||oh();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,f=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=f>>2,m=(f&3)<<4|h>>4,n=(h&15)<<2|k>>6,q=k&63;e||(q=64,d||(n=64));b.push(ph[l],ph[m],ph[n],ph[q])}return b.join("")}
function th(a){function b(l){for(;d<a.length;){var m=a.charAt(d++),n=rh[m];if(null!=n)return n;if(!/^[\s\xa0]*$/.test(m))throw Error("Unknown base64 encoding at char: "+m);}return l}ph=ph||qh();rh=rh||oh();for(var c="",d=0;;){var e=b(-1),f=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|f>>4);64!=h&&(c+=String.fromCharCode(f<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var uh;function vh(a,b){if(!a||b===F.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}
var zh=function(){var a=wh,b=xh,c=yh(),d=function(h){a(h.target||h.srcElement||{})},e=function(h){b(h.target||h.srcElement||{})};if(!c.init){nc(F,"mousedown",d);nc(F,"keyup",d);nc(F,"submit",e);var f=HTMLFormElement.prototype.submit;HTMLFormElement.prototype.submit=function(){b(this);f.call(this)};c.init=!0}},Ah=function(a,b,c){for(var d=yh().decorators,e={},f=0;f<d.length;++f){var h=d[f];if((!c||h.forms)&&vh(h.domains,a)){var k=h.placement;void 0==k&&(k=h.fragment?2:1);k===b&&Ia(e,h.callback())}}return e},
yh=function(){var a=hc("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var Bh=/(.*?)\*(.*?)\*(.*)/,Ch=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,Dh=/^(?:www\.|m\.|amp\.)+/,Eh=/([^?#]+)(\?[^#]*)?(#.*)?/,Gh=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(sh(String(d))))}var e=b.join("*");return["1",Fh(e),e].join("*")},Fh=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||window.navigator.language,Math.floor((new Date).getTime()/
60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=uh)){for(var e=Array(256),f=0;256>f;f++){for(var h=f,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[f]=h}d=e}uh=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^uh[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},Ih=function(){return function(a){var b=Ze(D.location.href),c=b.search.replace("?",""),d=Ve(c,"_gl",!0)||"";a.query=Hh(d)||{};var e=Ye(b,"fragment").match(/(.*?)(^|&)_gl=([^&]*)&?(.*)/);a.fragment=Hh(e&&e[3]||"")||{}}},Jh=function(){var a=
Ih(),b=yh();b.data||(b.data={query:{},fragment:{}},a(b.data));var c={},d=b.data;d&&(Ia(c,d.query),Ia(c,d.fragment));return c},Hh=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var f=Bh.exec(d);if(f){c=f;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],n=0;n<b;++n)if(m===Fh(k,n)){l=!0;break a}l=!1}if(l){for(var q={},u=k?k.split("*"):[],p=0;p<u.length;p+=2)q[u[p]]=th(u[p+1]);return q}}}}catch(t){}};
function Kh(a,b,c){function d(m){var n=m,q=/(.*?)(^|&)_gl=([^&]*)&?(.*)/.exec(n),u=n;if(q){var p=q[2],t=q[4];u=q[1];t&&(u=u+p+t)}m=u;var v=m.charAt(m.length-1);m&&"&"!==v&&(m+="&");return m+l}c=void 0===c?!1:c;var e=Eh.exec(b);if(!e)return"";var f=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+f+h+k}
function Lh(a,b){var c="FORM"===(a.tagName||"").toUpperCase(),d=Ah(b,1,c),e=Ah(b,2,c);if(Ja(d)){var f=Gh(d);if(c){if(a&&a.action){var h=(a.method||"").toLowerCase();if("get"===h){for(var k=a.childNodes||[],l=!1,m=0;m<k.length;m++){var n=k[m];if("_gl"===n.name){n.setAttribute("value",f);l=!0;break}}if(!l){var q=F.createElement("input");q.setAttribute("type","hidden");q.setAttribute("name","_gl");q.setAttribute("value",f);a.appendChild(q)}}else if("post"===h){var u=Kh(f,a.action);Te.test(u)&&(a.action=
u)}}}else Mh(f,a,!1)}if(!c&&Ja(e)){var p=Gh(e);Mh(p,a,!0)}}function Mh(a,b,c){if(b.href){var d=Kh(a,b.href,void 0===c?!1:c);Te.test(d)&&(b.href=d)}}
var wh=function(a){try{var b;a:{for(var c=a,d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var f=e.protocol;"http:"!==f&&"https:"!==f||Lh(e,e.hostname)}}catch(h){}},xh=function(a){try{if(a.action){var b=Ye(Ze(a.action),"host");Lh(a,b)}}catch(c){}},Nh=function(a,b,c,d){zh();var e="fragment"===c?2:1,f={callback:a,domains:b,fragment:2===e,placement:e,forms:!!d};yh().decorators.push(f)},Oh=function(){var a=F.location.hostname,b=Ch.exec(F.referrer);
if(!b)return!1;var c=b[2],d=b[1],e="";if(c){var f=c.split("/"),h=f[1];e="s"===h?decodeURIComponent(f[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}var k=a.replace(Dh,""),l=e.replace(Dh,""),m;if(!(m=k===l)){var n="."+l;m=k.substring(k.length-n.length,k.length)===n}return m},Ph=function(a,b){return!1===a?!1:a||b||Oh()};var Qh={};var Rh=/^\w+$/,Sh=/^[\w-]+$/,Th=/^~?[\w-]+$/,Uh={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha",gp:"_gp"};function Vh(a){return a&&"string"==typeof a&&a.match(Rh)?a:"_gcl"}
var Xh=function(){var a=Ze(D.location.href),b=Ye(a,"query",!1,void 0,"gclid"),c=Ye(a,"query",!1,void 0,"gclsrc"),d=Ye(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||Ve(e,"gclid",void 0);c=c||Ve(e,"gclsrc",void 0)}return Wh(b,c,d)},Wh=function(a,b,c){var d={},e=function(f,h){d[h]||(d[h]=[]);d[h].push(f)};d.gclid=a;d.gclsrc=b;d.dclid=c;if(void 0!==a&&a.match(Sh))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "3p.ds":(void 0==
Qh.gtm_3pds?0:Qh.gtm_3pds)&&e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha");break;case "gp":e(a,"gp")}c&&e(c,"dc");return d},Zh=function(a){var b=Xh();Yh(b,a)};
function Yh(a,b,c){function d(q,u){var p=$h(q,e);p&&Zf(p,u,h,f,l,!0)}b=b||{};var e=Vh(b.prefix),f=b.domain||"auto",h=b.path||"/",k=void 0==b.Ka?7776E3:b.Ka;c=c||Fa();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),n=function(q){return["GCL",m,q].join(".")};a.aw&&(!0===b.Lh?d("aw",n("~"+a.aw[0])):d("aw",n(a.aw[0])));a.dc&&d("dc",n(a.dc[0]));a.gf&&d("gf",n(a.gf[0]));a.ha&&d("ha",n(a.ha[0]));a.gp&&d("gp",n(a.gp[0]))}
var bi=function(a,b,c,d,e){for(var f=Jh(),h=Vh(b),k=0;k<a.length;++k){var l=a[k];if(void 0!==Uh[l]){var m=$h(l,h),n=f[m];if(n){var q=Math.min(ai(n),Fa()),u;b:{for(var p=q,t=Rf(m,F.cookie),v=0;v<t.length;++v)if(ai(t[v])>p){u=!0;break b}u=!1}u||Zf(m,n,c,d,0==e?void 0:new Date(q+1E3*(null==e?7776E3:e)),!0)}}}var w={prefix:b,path:c,domain:d};Yh(Wh(f.gclid,f.gclsrc),w)},$h=function(a,b){var c=Uh[a];if(void 0!==c)return b+c},ai=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||
0)};function ci(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var di=function(a,b,c,d,e){if(ua(b)){var f=Vh(e);Nh(function(){for(var h={},k=0;k<a.length;++k){var l=$h(a[k],f);if(l){var m=Rf(l,F.cookie);m.length&&(h[l]=m.sort()[m.length-1])}}return h},b,c,d)}},ei=function(a){return a.filter(function(b){return Th.test(b)})},fi=function(a,b){for(var c=Vh(b&&b.prefix),d={},e=0;e<a.length;e++)Uh[a[e]]&&(d[a[e]]=Uh[a[e]]);za(d,function(f,h){var k=Rf(c+h,F.cookie);if(k.length){var l=k[0],m=ai(l),n={};n[f]=[ci(l)];Yh(n,b,m)}})};var gi=/^\d+\.fls\.doubleclick\.net$/;function hi(a){var b=Ze(D.location.href),c=Ye(b,"host",!1);if(c&&c.match(gi)){var d=Ye(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function ii(a,b){if("aw"==a||"dc"==a){var c=hi("gcl"+a);if(c)return c.split(".")}var d=Vh(b);if("_gcl"==d){var e;e=Xh()[a]||[];if(0<e.length)return e}var f=$h(a,d),h;if(f){var k=[];if(F.cookie){var l=Rf(f,F.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var n=ci(l[m]);n&&-1===r(k,n)&&k.push(n)}h=ei(k)}else h=k}else h=k}else h=[];return h}
var ji=function(){var a=hi("gac");if(a)return decodeURIComponent(a);var b=nh(),c=[];za(b,function(d,e){for(var f=[],h=0;h<e.length;h++)f.push(e[h].$f);f=ei(f);f.length&&c.push(d+":"+f.join(","))});return c.join(";")},ki=function(a,b,c,d,e){mh(b,c,d,e);var f=ih[jh(b)],h=Xh().dc||[],k=!1;if(f&&0<h.length){var l=Vc.joined_au=Vc.joined_au||{},m=b||"_gcl";if(!l[m])for(var n=0;n<h.length;n++){var q="https://adservice.google.com/ddm/regclk",u=q=q+"?gclid="+h[n]+"&auiddc="+f;fc.sendBeacon&&fc.sendBeacon(u)||mc(u);k=l[m]=
!0}}null==a&&(a=k);if(a&&f){var p=jh(b),t=ih[p];t&&lh(p,t,c,d,e)}};var li;if(3===Uc.yb.length)li="g";else{var mi="G";li=mi}
var ni={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:li,OPT:"o"},oi=function(a){var b=Uc.s.split("-"),c=b[0].toUpperCase(),d=ni[c]||"i",e=a&&"GTM"===c?b[1]:"OPT"===c?b[1]:"",f;if(3===Uc.yb.length){var h=void 0;f="2"+(h||"w")}else f=
"";return f+d+Uc.yb+e};
var pi=function(a){return!(void 0===a||null===a||0===(a+"").length)},qi=function(a,b){var c;if(2===b.V)return a("ord",wa(1E11,1E13)),!0;if(3===b.V)return a("ord","1"),a("num",wa(1E11,1E13)),!0;if(4===b.V)return pi(b.sessionId)&&a("ord",b.sessionId),!0;if(5===b.V)c="1";else if(6===b.V)c=b.Yc;else return!1;pi(c)&&a("qty",c);pi(b.Gb)&&a("cost",b.Gb);pi(b.transactionId)&&a("ord",b.transactionId);return!0},ri=encodeURIComponent,si=function(a,b){function c(n,q,u){f.hasOwnProperty(n)||(q+="",e+=";"+n+"="+
(u?q:ri(q)))}var d=a.Ec,e=a.protocol;e+=a.Zb?"//"+d+".fls.doubleclick.net/activityi":"//ad.doubleclick.net/activity";e+=";src="+ri(d)+(";type="+ri(a.Hc))+(";cat="+ri(a.fb));var f=a.Rf||{};za(f,function(n,q){e+=";"+ri(n)+"="+ri(q+"")});if(qi(c,a)){pi(a.hc)&&c("u",a.hc);pi(a.fc)&&c("tran",a.fc);c("gtm",oi());!1===a.vf&&c("npa","1");if(a.Dc){var h=ii("dc",a.Fa);h&&h.length&&c("gcldc",h.join("."));var k=ii("aw",a.Fa);k&&k.length&&c("gclaw",k.join("."));var l=ji();l&&c("gac",l);mh(a.Fa,void 0,a.Nf,a.Of);
var m=ih[jh(a.Fa)];m&&c("auiddc",m)}pi(a.Uc)&&c("prd",a.Uc,!0);za(a.fd,function(n,q){c(n,q)});e+=b||"";pi(a.Ub)&&c("~oref",a.Ub);a.Zb?lc(e+"?",a.B):mc(e+"?",a.B,a.w)}else G(a.w)};var ti=["input","select","textarea"],ui=["button","hidden","image","reset","submit"],vi=function(a){var b=a.tagName.toLowerCase();return!va(ti,function(c){return c===b})||"input"===b&&va(ui,function(c){return c===a.type.toLowerCase()})?!1:!0},wi=function(a){return a.form?a.form.tagName?a.form:F.getElementById(a.form):tc(a,["form"],100)},xi=function(a,b,c){if(!a.elements)return 0;for(var d=b.getAttribute(c),e=0,f=1;e<a.elements.length;e++){var h=a.elements[e];if(vi(h)){if(h.getAttribute(c)===d)return f;
f++}}return 0};var Ai=!!D.MutationObserver,Bi=void 0,Ci=function(a){if(!Bi){var b=function(){var c=F.body;if(c)if(Ai)(new MutationObserver(function(){for(var e=0;e<Bi.length;e++)G(Bi[e])})).observe(c,{childList:!0,subtree:!0});else{var d=!1;nc(c,"DOMNodeInserted",function(){d||(d=!0,G(function(){d=!1;for(var e=0;e<Bi.length;e++)G(Bi[e])}))})}};Bi=[];F.body?b():G(b)}Bi.push(a)};
var Ni=function(){var a=F.body,b=F.documentElement||a&&a.parentElement,c,d;if(F.compatMode&&"BackCompat"!==F.compatMode)c=b?b.clientHeight:0,d=b?b.clientWidth:0;else{var e=function(f,h){return f&&h?Math.min(f,h):Math.max(f,h)};I("GTM",7);c=e(b?b.clientHeight:0,a?a.clientHeight:0);d=e(b?b.clientWidth:0,a?a.clientWidth:0)}return{width:d,height:c}},Oi=function(a){var b=Ni(),c=b.height,d=b.width,e=a.getBoundingClientRect(),f=e.bottom-e.top,h=e.right-e.left;return f&&h?(1-Math.min((Math.max(0-e.left,0)+
Math.max(e.right-d,0))/h,1))*(1-Math.min((Math.max(0-e.top,0)+Math.max(e.bottom-c,0))/f,1)):0},Pi=function(a){if(F.hidden)return!0;var b=a.getBoundingClientRect();if(b.top==b.bottom||b.left==b.right||!D.getComputedStyle)return!0;var c=D.getComputedStyle(a,null);if("hidden"===c.visibility)return!0;for(var d=a,e=c;d;){if("none"===e.display)return!0;var f=e.opacity,h=e.filter;if(h){var k=h.indexOf("opacity(");0<=k&&(h=h.substring(k+8,h.indexOf(")",k)),"%"==h.charAt(h.length-1)&&(h=h.substring(0,h.length-
1)),f=Math.min(h,f))}if(void 0!==f&&0>=f)return!0;(d=d.parentElement)&&(e=D.getComputedStyle(d,null))}return!1};var Yi=D.clearTimeout,Zi=D.setTimeout,R=function(a,b,c){if(Ud()){b&&G(b)}else return jc(a,b,c)},$i=function(){return D.location.href},aj=function(a){return Ye(Ze(a),"fragment")},bj=function(a){return Xe(Ze(a))},U=function(a,b){return Kd(a,b||2)},cj=function(a,b,c){var d;b?(a.eventCallback=b,c&&(a.eventTimeout=c),d=Ng(a)):d=Ng(a);return d},dj=function(a,b){D[a]=b},X=function(a,b,c){b&&(void 0===D[a]||c&&!D[a])&&(D[a]=
b);return D[a]},ej=function(a,b,c){return Rf(a,b,void 0===c?!0:!!c)},fj=function(a,b){if(Ud()){b&&G(b)}else lc(a,b)},gj=function(a){return!!bh(a,"init",!1)},hj=function(a){$g(a,"init",!0)},ij=function(a,b){var c=(void 0===b?0:b)?"www.googletagmanager.com/gtag/js":Zc;c+="?id="+encodeURIComponent(a)+"&l=dataLayer";R(Q("https://","http://",c))},jj=function(a,b){var c=a[b];return c};
var kj=Vg.xg;var lj;var Ij=new xa;function Jj(a,b){function c(h){var k=Ze(h),l=Ye(k,"protocol"),m=Ye(k,"host",!0),n=Ye(k,"port"),q=Ye(k,"path").toLowerCase().replace(/\/$/,"");if(void 0===l||"http"==l&&"80"==n||"https"==l&&"443"==n)l="web",n="default";return[l,m,n,q]}for(var d=c(String(a)),e=c(String(b)),f=0;f<d.length;f++)if(d[f]!==e[f])return!1;return!0}
function Kj(a){return Lj(a)?1:0}
function Lj(a){var b=a.arg0,c=a.arg1;if(a.any_of&&ua(c)){for(var d=0;d<c.length;d++)if(Kj({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=String(b).indexOf(String(c));case "_css":var e;a:{if(b){var f=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<f.length;h++)if(b[f[h]]){e=b[f[h]](c);break a}}catch(v){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-
l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var n;n=String(b).split(",");return 0<=r(n,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var q;var u=a.ignore_case?"i":void 0;try{var p=String(c)+u,t=Ij.get(p);t||(t=new RegExp(c,u),Ij.set(p,t));q=t.test(b)}catch(v){q=!1}return q;case "_sw":return 0==String(b).indexOf(String(c));case "_um":return Jj(b,
c)}return!1};var Mj=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var Nj={},Oj=encodeURI,Y=encodeURIComponent,Pj=mc;var Qj=function(a,b){if(!a)return!1;var c=Ye(Ze(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var f=c.length-e.length;0<f&&"."!=e.charAt(0)&&(f--,e="."+e);if(0<=f&&c.indexOf(e,f)==f)return!0}}return!1};
var Rj=function(a,b,c){for(var d={},e=!1,f=0;a&&f<a.length;f++)a[f]&&a[f].hasOwnProperty(b)&&a[f].hasOwnProperty(c)&&(d[a[f][b]]=a[f][c],e=!0);return e?d:null};Nj.ng=function(){var a=!1;return a};var dl=function(){var a=D.gaGlobal=D.gaGlobal||{};a.hid=a.hid||wa();return a.hid};var ol=window,pl=document,ql=function(a){var b=ol._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===ol["ga-disable-"+a])return!0;try{var c=ol.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(f){}for(var d=Rf("AMP_TOKEN",pl.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return pl.getElementById("__gaOptOutExtension")?!0:!1};var tl=function(a){za(a,function(c){"_"===c.charAt(0)&&delete a[c]});var b=a[H.ca]||{};za(b,function(c){"_"===c.charAt(0)&&delete b[c]})};var xl=function(a,b,c){Kf(b,c,a)},yl=function(a,b,c){Kf(b,c,a,!0)},Al=function(a,b){};
function zl(a,b){}var Z={a:{}};

Z.a.sdl=["google"],function(){function a(){return!!(Object.keys(l("horiz.pix")).length||Object.keys(l("horiz.pct")).length||Object.keys(l("vert.pix")).length||Object.keys(l("vert.pct")).length)}function b(x){for(var z=[],C=x.split(","),A=0;A<C.length;A++){var E=Number(C[A]);if(isNaN(E))return[];n.test(C[A])||z.push(E)}return z}function c(){var x=0,z=0;return function(){var C=Ni(),A=C.height;x=Math.max(v.scrollLeft+C.width,x);z=Math.max(v.scrollTop+A,z);return{Sf:x,Tf:z}}}function d(){p=X("self");
t=p.document;v=t.scrollingElement||t.body&&t.body.parentNode;y=c()}function e(x,z,C,A){var E=l(z),J={},M;for(M in E){J.za=M;if(E.hasOwnProperty(J.za)){var V=Number(J.za);x<V||(cj({event:"gtm.scrollDepth","gtm.scrollThreshold":V,"gtm.scrollUnits":C.toLowerCase(),"gtm.scrollDirection":A,"gtm.triggers":E[J.za].join(",")}),ah("sdl",z,function(W){return function(S){delete S[W.za];return S}}(J),{}))}J={za:J.za}}}function f(){var x=y(),z=x.Sf,C=x.Tf,A=z/v.scrollWidth*100,E=C/v.scrollHeight*100;e(z,"horiz.pix",
q.wb,u.wd);e(A,"horiz.pct",q.vb,u.wd);e(C,"vert.pix",q.wb,u.Sd);e(E,"vert.pct",q.vb,u.Sd);$g("sdl","pending",!1)}function h(){var x=250,z=!1;t.scrollingElement&&t.documentElement&&p.addEventListener&&(x=50,z=!0);var C=0,A=!1,E=function(){A?C=Zi(E,x):(C=0,f(),gj("sdl")&&!a()&&(oc(p,"scroll",J),oc(p,"resize",J),$g("sdl","init",!1)));A=!1},J=function(){z&&y();C?A=!0:(C=Zi(E,x),$g("sdl","pending",!0))};return J}function k(x,z,C){if(z){var A=b(String(x));ah("sdl",C,function(E){for(var J=0;J<A.length;J++){var M=
String(A[J]);E.hasOwnProperty(M)||(E[M]=[]);E[M].push(z)}return E},{})}}function l(x){return bh("sdl",x,{})}function m(x){G(x.vtp_gtmOnSuccess);var z=x.vtp_uniqueTriggerId,C=x.vtp_horizontalThresholdsPixels,A=x.vtp_horizontalThresholdsPercent,E=x.vtp_verticalThresholdUnits,J=x.vtp_verticalThresholdsPixels,M=x.vtp_verticalThresholdsPercent;switch(x.vtp_horizontalThresholdUnits){case q.wb:k(C,z,"horiz.pix");break;case q.vb:k(A,z,"horiz.pct")}switch(E){case q.wb:k(J,z,"vert.pix");break;case q.vb:k(M,
z,"vert.pct")}gj("sdl")?bh("sdl","pending")||(w||(d(),w=!0),G(function(){return f()})):(d(),w=!0,v&&(hj("sdl"),$g("sdl","pending",!0),G(function(){f();if(a()){var V=h();nc(p,"scroll",V);nc(p,"resize",V)}else $g("sdl","init",!1)})))}var n=/^\s*$/,q={vb:"PERCENT",wb:"PIXELS"},u={Sd:"vertical",wd:"horizontal"},p,t,v,w=!1,y;(function(x){Z.__sdl=x;Z.__sdl.b="sdl";Z.__sdl.g=!0;Z.__sdl.priorityOverride=0})(function(x){x.vtp_triggerStartOption?m(x):vg(function(){m(x)})})}();

Z.a.jsm=["customScripts"],function(){(function(a){Z.__jsm=a;Z.__jsm.b="jsm";Z.__jsm.g=!0;Z.__jsm.priorityOverride=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=X("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();
Z.a.flc=[],function(){function a(b,c){c=c?c.slice(0,-1):void 0;si(b,c)}(function(b){Z.__flc=b;Z.__flc.b="flc";Z.__flc.g=!0;Z.__flc.priorityOverride=0})(function(b){var c=!b.hasOwnProperty("vtp_enableConversionLinker")||b.vtp_enableConversionLinker,d=Rj(b.vtp_customVariable||[],"key","value")||{},e={fb:b.vtp_activityTag,Dc:c,Fa:b.vtp_conversionCookiePrefix||void 0,Gb:void 0,V:{UNIQUE:3,SESSION:4}[b.vtp_ordinalType]||2,Ec:b.vtp_advertiserId,Hc:b.vtp_groupTag,w:b.vtp_gtmOnFailure,B:b.vtp_gtmOnSuccess,
Ub:b.vtp_useImageTag?void 0:b.vtp_url,protocol:"",Yc:void 0,Zb:!b.vtp_useImageTag,sessionId:b.vtp_sessionId,fc:b.vtp_transactionVariable,transactionId:void 0,hc:b.vtp_userVariable,fd:d};if(b.vtp_enableAttribution){var f=b.vtp_attributionFields||[];if(f.length){R("//www.gstatic.com/attribution/collection/attributiontools.js",function(){a(e,X("google_attr").build([Rj(f,"key","value")||{}]))},b.vtp_gtmOnFailure);return}}a(e)})}();
Z.a.sp=["google"],function(){(function(a){Z.__sp=a;Z.__sp.b="sp";Z.__sp.g=!0;Z.__sp.priorityOverride=0})(function(a){var b=-1==navigator.userAgent.toLowerCase().indexOf("firefox")?"//www.googleadservices.com/pagead/conversion_async.js":"https://www.google.com/pagead/conversion_async.js",c=a.vtp_gtmOnFailure;He();R(b,function(){var d=X("google_trackConversion");if(qa(d)){var e={};"DATA_LAYER"==a.vtp_customParamsFormat?e=a.vtp_dataLayerVariable:"USER_SPECIFIED"==a.vtp_customParamsFormat&&(e=Rj(a.vtp_customParams,
"key","value"));var f={};a.vtp_enableDynamicRemarketing&&(a.vtp_eventName&&(e.event=a.vtp_eventName),a.vtp_eventValue&&(f.value=a.vtp_eventValue),a.vtp_eventItems&&(f.items=a.vtp_eventItems));var h={google_conversion_id:a.vtp_conversionId,google_conversion_label:a.vtp_conversionLabel,google_custom_params:e,google_gtag_event_data:f,google_remarketing_only:!0,onload_callback:a.vtp_gtmOnSuccess,google_gtm:oi()};a.vtp_rdp&&(h.google_restricted_data_processing=!0);d(h)||c()}else c()},c)})}();

Z.a.d=["google"],function(){(function(a){Z.__d=a;Z.__d.b="d";Z.__d.g=!0;Z.__d.priorityOverride=0})(function(a){var b=null,c=null,d=a.vtp_attributeName;if("CSS"==a.vtp_selectorType){var e=wc(a.vtp_elementSelector);e&&0<e.length&&(b=e[0])}else b=F.getElementById(a.vtp_elementId);b&&(d?c=qc(b,d):c=rc(b));return Ea(String(b&&c))})}();
Z.a.e=["google"],function(){(function(a){Z.__e=a;Z.__e.b="e";Z.__e.g=!0;Z.__e.priorityOverride=0})(function(a){return String(Sd(a.vtp_gtmEventId,"event"))})}();
Z.a.f=["google"],function(){(function(a){Z.__f=a;Z.__f.b="f";Z.__f.g=!0;Z.__f.priorityOverride=0})(function(a){var b=U("gtm.referrer",1)||F.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?Ye(Ze(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):bj(String(b)):String(b)})}();
Z.a.cl=["google"],function(){function a(b){var c=b.target;if(c){var d=Yg(c,"gtm.click");cj(d)}}(function(b){Z.__cl=b;Z.__cl.b="cl";Z.__cl.g=!0;Z.__cl.priorityOverride=0})(function(b){if(!gj("cl")){var c=X("document");nc(c,"click",a,!0);hj("cl")}G(b.vtp_gtmOnSuccess)})}();
Z.a.j=["google"],function(){(function(a){Z.__j=a;Z.__j.b="j";Z.__j.g=!0;Z.__j.priorityOverride=0})(function(a){for(var b=String(a.vtp_name).split("."),c=X(b.shift()),d=0;d<b.length;d++)c=c&&c[b[d]];return c})}();Z.a.k=["google"],function(){(function(a){Z.__k=a;Z.__k.b="k";Z.__k.g=!0;Z.__k.priorityOverride=0})(function(a){return ej(a.vtp_name,U("gtm.cookie",1),!!a.vtp_decodeCookie)[0]})}();
Z.a.r=["google"],function(){(function(a){Z.__r=a;Z.__r.b="r";Z.__r.g=!0;Z.__r.priorityOverride=0})(function(a){return wa(a.vtp_min,a.vtp_max)})}();
Z.a.u=["google"],function(){var a=function(b){return{toString:function(){return b}}};(function(b){Z.__u=b;Z.__u.b="u";Z.__u.g=!0;Z.__u.priorityOverride=0})(function(b){var c;b.vtp_customUrlSource?c=b.vtp_customUrlSource:c=U("gtm.url",1);c=c||$i();var d=b[a("vtp_component")];if(!d||"URL"==d)return bj(String(c));var e=Ze(String(c)),f;if("QUERY"===d)a:{var h=b[a("vtp_multiQueryKeys").toString()],k=b[a("vtp_queryKey").toString()]||"",l=b[a("vtp_ignoreEmptyQueryParam").toString()],m;h?ua(k)?m=k:m=String(k).replace(/\s+/g,
"").split(","):m=[String(k)];for(var n=0;n<m.length;n++){var q=Ye(e,"QUERY",void 0,void 0,m[n]);if(void 0!=q&&(!l||""!==q)){f=q;break a}}f=void 0}else f=Ye(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,void 0);return f})}();
Z.a.v=["google"],function(){(function(a){Z.__v=a;Z.__v.b="v";Z.__v.g=!0;Z.__v.priorityOverride=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=U(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();
Z.a.tl=["google"],function(){function a(b){return function(){if(b.Oc&&b.Qc>=b.Oc)b.Kc&&X("self").clearInterval(b.Kc);else{b.Qc++;var c=(new Date).getTime();cj({event:b.Z,"gtm.timerId":b.Kc,"gtm.timerEventNumber":b.Qc,"gtm.timerInterval":b.interval,"gtm.timerLimit":b.Oc,"gtm.timerStartTime":b.De,"gtm.timerCurrentTime":c,"gtm.timerElapsedTime":c-b.De,"gtm.triggers":b.fh})}}}(function(b){Z.__tl=b;Z.__tl.b="tl";Z.__tl.g=!0;Z.__tl.priorityOverride=0})(function(b){G(b.vtp_gtmOnSuccess);if(!isNaN(b.vtp_interval)){var c=
{Z:b.vtp_eventName,Qc:0,interval:Number(b.vtp_interval),Oc:isNaN(b.vtp_limit)?0:Number(b.vtp_limit),fh:String(b.vtp_uniqueTriggerId||"0"),De:(new Date).getTime()};c.Kc=X("self").setInterval(a(c),0>Number(b.vtp_interval)?0:Number(b.vtp_interval))}})}();
Z.a.asp=["nonGoogleScripts"],function(){(function(a){Z.__asp=a;Z.__asp.b="asp";Z.__asp.g=!0;Z.__asp.priorityOverride=0})(function(a){D.adroll_adv_id=a.vtp_customerId;D.adroll_pix_id=a.vtp_pixelId;void 0!==a.vtp_conversionValueInDollars&&(D.adroll_conversion_value=a.vtp_conversionValueInDollars,D.adroll_currency=a.vtp_conversionValueCurrency||"USD");if(a.vtp_customData){var b=Rj(a.vtp_customData,"key","value");D.adroll_custom_data=b}a.vtp_segmentName&&(D.adroll_segments=a.vtp_segmentName);a.vtp_visitorEmail&&
(D.adroll_email=a.vtp_visitorEmail);D.__adroll_loaded=!0;var c=Q("https://s","http://a",".adroll.com/j/roundtrip.js");R(c,a.vtp_gtmOnSuccess,a.vtp_gtmOnFailure)})}();
Z.a.ua=["google"],function(){var a,b={},c=function(d){var e={},f={},h={},k={},l={},m=void 0;if(d.vtp_gaSettings){var n=d.vtp_gaSettings;B(Rj(n.vtp_fieldsToSet,"fieldName","value"),f);B(Rj(n.vtp_contentGroup,"index","group"),h);B(Rj(n.vtp_dimension,"index","dimension"),k);B(Rj(n.vtp_metric,"index","metric"),l);d.vtp_gaSettings=null;n.vtp_fieldsToSet=void 0;n.vtp_contentGroup=void 0;n.vtp_dimension=void 0;n.vtp_metric=void 0;var q=B(n);d=B(d,q)}B(Rj(d.vtp_fieldsToSet,"fieldName","value"),f);B(Rj(d.vtp_contentGroup,
"index","group"),h);B(Rj(d.vtp_dimension,"index","dimension"),k);B(Rj(d.vtp_metric,"index","metric"),l);var u=Oe(d.vtp_functionName);if(qa(u)){var p="",t="";d.vtp_setTrackerName&&"string"==typeof d.vtp_trackerName?""!==d.vtp_trackerName&&(t=d.vtp_trackerName,p=t+"."):(t="gtm"+gd(),p=t+".");var v={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,
legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},w={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0,allowAdPersonalizationSignals:!0},y=function(O){var K=[].slice.call(arguments,0);K[0]=p+K[0];u.apply(window,K)},x=function(O,K){return void 0===K?K:O(K)},z=function(O,K){if(K)for(var sa in K)K.hasOwnProperty(sa)&&
y("set",O+sa,K[sa])},C=function(){},A=function(O,K,sa){var Hb=0;if(O)for(var Da in O)if(O.hasOwnProperty(Da)&&(sa&&v[Da]||!sa&&void 0===v[Da])){var $a=w[Da]?Ba(O[Da]):O[Da];"anonymizeIp"!=Da||$a||($a=void 0);K[Da]=$a;Hb++}return Hb},E={name:t};A(f,E,
!0);u("create",d.vtp_trackingId||e.trackingId,E);y("set","&gtm",oi(!0));d.vtp_enableRecaptcha&&y("require","recaptcha","recaptcha.js");(function(O,K){void 0!==d[K]&&y("set",O,d[K])})("nonInteraction","vtp_nonInteraction");z("contentGroup",h);z("dimension",k);z("metric",l);var J={};A(f,J,!1)&&y("set",J);var M;d.vtp_enableLinkId&&y("require","linkid","linkid.js");y("set","hitCallback",function(){var O=f&&f.hitCallback;qa(O)&&O();d.vtp_gtmOnSuccess()});if("TRACK_EVENT"==d.vtp_trackType){d.vtp_enableEcommerce&&(y("require","ec","ec.js"),C());var V={hitType:"event",eventCategory:String(d.vtp_eventCategory||e.category),eventAction:String(d.vtp_eventAction||
e.action),eventLabel:x(String,d.vtp_eventLabel||e.label),eventValue:x(Aa,d.vtp_eventValue||e.value)};A(M,V,!1);y("send",V);}else if("TRACK_SOCIAL"==d.vtp_trackType){}else if("TRACK_TRANSACTION"==
d.vtp_trackType){}else if("TRACK_TIMING"==d.vtp_trackType){}else if("DECORATE_LINK"==
d.vtp_trackType){}else if("DECORATE_FORM"==d.vtp_trackType){}else if("TRACK_DATA"==d.vtp_trackType){}else{d.vtp_enableEcommerce&&
(y("require","ec","ec.js"),C());if(d.vtp_doubleClick||"DISPLAY_FEATURES"==d.vtp_advertisingFeaturesType){var oa="_dc_gtm_"+String(d.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");y("require","displayfeatures",void 0,{cookieName:oa})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==d.vtp_advertisingFeaturesType){var ka="_dc_gtm_"+String(d.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");y("require","adfeatures",{cookieName:ka})}M?y("send","pageview",M):y("send","pageview");}if(!a){var ta=d.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";d.vtp_useInternalVersion&&!d.vtp_useDebugVersion&&(ta="internal/"+ta);a=!0;var bb=Q("https:","http:","//www.google-analytics.com/"+ta,f&&f.forceSSL);
R(bb,function(){var O=Me();O&&O.loaded||d.vtp_gtmOnFailure();},d.vtp_gtmOnFailure)}}else G(d.vtp_gtmOnFailure)};Z.__ua=c;Z.__ua.b="ua";Z.__ua.g=!0;Z.__ua.priorityOverride=0}();







Z.a.aev=["google"],function(){function a(p,t){var v=Sd(p,"gtm");if(v)return v[t]}function b(p,t,v,w){w||(w="element");var y=p+"."+t,x;if(n.hasOwnProperty(y))x=n[y];else{var z=a(p,w);if(z&&(x=v(z),n[y]=x,q.push(y),35<q.length)){var C=q.shift();delete n[C]}}return x}function c(p,t,v){var w=a(p,u[t]);return void 0!==w?w:v}function d(p,t){if(!p)return!1;var v=e($i());ua(t)||(t=String(t||"").replace(/\s+/g,"").split(","));for(var w=[v],y=0;y<t.length;y++)if(t[y]instanceof RegExp){if(t[y].test(p))return!1}else{var x=
t[y];if(0!=x.length){if(0<=e(p).indexOf(x))return!1;w.push(e(x))}}return!Qj(p,w)}function e(p){m.test(p)||(p="http://"+p);return Ye(Ze(p),"HOST",!0)}function f(p,t,v){switch(p){case "SUBMIT_TEXT":return b(t,"FORM."+p,h,"formSubmitElement")||v;case "LENGTH":var w=b(t,"FORM."+p,k);return void 0===w?v:w;case "INTERACTED_FIELD_ID":return l(t,"id",v);case "INTERACTED_FIELD_NAME":return l(t,"name",v);case "INTERACTED_FIELD_TYPE":return l(t,"type",v);case "INTERACTED_FIELD_POSITION":var y=a(t,"interactedFormFieldPosition");
return void 0===y?v:y;case "INTERACT_SEQUENCE_NUMBER":var x=a(t,"interactSequenceNumber");return void 0===x?v:x;default:return v}}function h(p){switch(p.tagName.toLowerCase()){case "input":return qc(p,"value");case "button":return rc(p);default:return null}}function k(p){if("form"===p.tagName.toLowerCase()&&p.elements){for(var t=0,v=0;v<p.elements.length;v++)vi(p.elements[v])&&t++;return t}}function l(p,t,v){var w=a(p,"interactedFormField");return w&&qc(w,t)||v}var m=/^https?:\/\//i,n={},q=[],u={ATTRIBUTE:"elementAttribute",
CLASSES:"elementClasses",ELEMENT:"element",ID:"elementId",HISTORY_CHANGE_SOURCE:"historyChangeSource",HISTORY_NEW_STATE:"newHistoryState",HISTORY_NEW_URL_FRAGMENT:"newUrlFragment",HISTORY_OLD_STATE:"oldHistoryState",HISTORY_OLD_URL_FRAGMENT:"oldUrlFragment",TARGET:"elementTarget"};(function(p){Z.__aev=p;Z.__aev.b="aev";Z.__aev.g=!0;Z.__aev.priorityOverride=0})(function(p){var t=p.vtp_gtmEventId,v=p.vtp_defaultValue,w=p.vtp_varType;switch(w){case "TAG_NAME":var y=a(t,"element");return y&&y.tagName||
v;case "TEXT":return b(t,w,rc)||v;case "URL":var x;a:{var z=String(a(t,"elementUrl")||v||""),C=Ze(z),A=String(p.vtp_component||"URL");switch(A){case "URL":x=z;break a;case "IS_OUTBOUND":x=d(z,p.vtp_affiliatedDomains);break a;default:x=Ye(C,A,p.vtp_stripWww,p.vtp_defaultPages,p.vtp_queryKey)}}return x;case "ATTRIBUTE":var E;if(void 0===p.vtp_attribute)E=c(t,w,v);else{var J=p.vtp_attribute,M=a(t,"element");E=M&&qc(M,J)||v||""}return E;case "MD":var V=p.vtp_mdValue,W=b(t,"MD",Ji);return V&&W?Mi(W,V)||
v:W||v;case "FORM":return f(String(p.vtp_component||"SUBMIT_TEXT"),t,v);default:return c(t,w,v)}})}();
Z.a.ga=["google"],function(){var a,b,c=function(d){function e(N){var L=[].slice.call(arguments,0);L[0]=v+L[0];p.push(L)}function f(N,L){void 0!==d[L]&&e(N,d[L])}function h(N,L){void 0!==d[L]&&e(N,Number(d[L]))}function k(N,L){if(u(L))for(var P=0;P<L.length;P++)e(N,L[P])}function l(N,L,P){if(u(L))for(var T=0;T<L.length;T++){for(var oa=[N],ka=L[T],ta=0;ta<P.length;ta++)oa.push(ka[P[ta]]);e.apply(this,oa)}}function m(N,L){void 0!==d[L]&&e("_set",N,d[L])}function n(N,L){return void 0===L?L:N(L)}function q(N,
L){void 0!==L&&(w+="&"+N+"="+L)}var u=ua,p=X("_gaq",[],!1),t=!1,v="";void 0==d.vtp_trackerName?v="gtm"+gd()+".":""!==d.vtp_trackerName&&(v=d.vtp_trackerName+".");f("_setAccount","vtp_webPropertyId");e("_set","gtmid",oi(!0));



var w="";

if(""!==w){var y=function(N,L,P){for(var T=L.length-1;0<=T;T--){var oa=L[T].split("=");if(oa[0]===N)return oa[1]||"1"}for(var ka=P.length-1;0<=ka;ka--){var ta=P[ka].split("=");if(ta[0]===N)return ta[1]||"1"}},x=Ze($i()),z=Ye(x,"query").split("&"),C=d.vtp_allowAnchor?Ye(x,"fragment").split("&"):[];q("gclid",y("gclid",C,z));q("gclsrc",y("gclsrc",C,z));q("dclid",y("dclid",C,z));e("_set","campaignParams",w)}d.vtp_enableInPageLinkId&&e("_require","inpage_linkid","//www.google-analytics.com/plugins/ga/inpage_linkid.js");l("_setPageGroup",d.vtp_contentGroup,["index","group"]);f("_setCampaignTrack",
"vtp_campaignTrack");f("_setClientInfo","vtp_clientInfo");f("_setDetectFlash","vtp_detectFlash");f("_setDetectTitle","vtp_detectTitle");

void 0!==d.vtp_forceSsl&&d.vtp_forceSsl&&(p.push(["_gat._forceSSL"]),t=!!d.vtp_forceSsl);e("_set","hitCallback",function(){qa(d.vtp_hitCallback)&&d.vtp_hitCallback();d.vtp_gtmOnSuccess()});if("TRACK_EVENT"==d.vtp_trackType){}else if("TRACK_TRANSACTION"==d.vtp_trackType){}else if("LINK"==d.vtp_trackType){}else if("LINK_BY_POST"==d.vtp_trackType){}else if("TRACK_SOCIAL"==d.vtp_trackType){}else if("TRACK_TIMING"==
d.vtp_trackType){}else if("DECORATE_LINK"==d.vtp_trackType){}else if("DECORATE_FORM"==d.vtp_trackType){}else e("_trackPageview");
var na=function(){X("_gat")||d.vtp_gtmOnFailure()};if(d.vtp_doubleClick)b||(b=!0,R(Q("https","http","://stats.g.doubleclick.net/dc.js",t),na,d.vtp_gtmOnFailure));else if(!a){var ha=d.vtp_useDebugVersion?".google-analytics.com/u/ga_debug.js":".google-analytics.com/ga.js";a=!0;R(Q("https://ssl","http://www",ha,t),na,d.vtp_gtmOnFailure)}};Z.__ga=c;Z.__ga.b="ga";Z.__ga.g=!0;Z.__ga.priorityOverride=0}();

Z.a.cegg=["nonGoogleScripts"],function(){var a={};(function(b){Z.__cegg=b;Z.__cegg.b="cegg";Z.__cegg.g=!0;Z.__cegg.priorityOverride=0})(function(b){try{var c=b.vtp_usersNumericId;if(c)if(a.hasOwnProperty(c)&&!0===a[c])b.vtp_gtmOnSuccess();else{b.vtp_snapshotName&&(D.CE_SNAPSHOT_NAME=b.vtp_snapshotName);for(var d=c.toString();8>d.length;)d="0"+d;var e=d.replace(/(\d+)(\d{4})$/,"/pages/scripts/$1/$2.js");R("//script.crazyegg.com"+e+"?"+Math.floor((new Date).getTime()/36E5),b.vtp_gtmOnSuccess,b.vtp_gtmOnFailure);
a[c]=!0}else G(b.vtp_gtmOnFailure)}catch(f){G(b.vtp_gtmOnFailure)}})}();
Z.a.hl=["google"],function(){function a(f){return f.target&&f.target.location&&f.target.location.href?f.target.location.href:$i()}function b(f,h){nc(f,"hashchange",function(k){var l=a(k);h({source:"hashchange",state:null,url:bj(l),J:aj(l)})})}function c(f,h){nc(f,"popstate",function(k){var l=a(k);h({source:"popstate",state:k.state,url:bj(l),J:aj(l)})})}function d(f,h,k){var l=h.history,m=l[f];if(qa(m))try{l[f]=function(n,q,u){m.apply(l,[].slice.call(arguments,0));k({source:f,state:n,url:bj($i()),
J:aj($i())})}}catch(n){}}function e(){var f={source:null,state:X("history").state||null,url:bj($i()),J:aj($i())};return function(h){var k=f,l={};l[k.source]=!0;l[h.source]=!0;if(!l.popstate||!l.hashchange||k.J!=h.J){var m={event:"gtm.historyChange","gtm.historyChangeSource":h.source,"gtm.oldUrlFragment":f.J,"gtm.newUrlFragment":h.J,"gtm.oldHistoryState":f.state,"gtm.newHistoryState":h.state};m["gtm.oldUrl"]=f.url,m["gtm.newUrl"]=h.url;
f=h;cj(m)}}}(function(f){Z.__hl=f;Z.__hl.b="hl";Z.__hl.g=!0;Z.__hl.priorityOverride=0})(function(f){var h=X("self");if(!gj("hl")){var k=e();b(h,k);c(h,k);d("pushState",h,k);d("replaceState",h,k);hj("hl")}G(f.vtp_gtmOnSuccess)})}();
Z.a.awct=["google"],function(){var a=!1,b=[],c=function(k){var l=X("google_trackConversion"),m=k.gtm_onFailure;"function"==typeof l?l(k)||m():m()},d=function(){for(;0<b.length;)c(b.shift())},e=function(){return function(){d();a=!1}},f=function(){return function(){d();b={push:c};}},h=function(k){He();var l={google_basket_transaction_type:"purchase",google_conversion_domain:"",google_conversion_id:k.vtp_conversionId,google_conversion_label:k.vtp_conversionLabel,
google_conversion_value:k.vtp_conversionValue||0,google_remarketing_only:!1,onload_callback:k.vtp_gtmOnSuccess,gtm_onFailure:k.vtp_gtmOnFailure,google_gtm:oi()};k.vtp_rdp&&(l.google_restricted_data_processing=!0);var m=function(v){return function(w,y,x){var z="DATA_LAYER"==v?U(x):k[y];z&&(l[w]=z)}},n=m("JSON");n("google_conversion_currency","vtp_currencyCode");n("google_conversion_order_id","vtp_orderId");k.vtp_enableProductReporting&&(n=m(k.vtp_productReportingDataSource),n("google_conversion_merchant_id",
"vtp_awMerchantId","aw_merchant_id"),n("google_basket_feed_country","vtp_awFeedCountry","aw_feed_country"),n("google_basket_feed_language","vtp_awFeedLanguage","aw_feed_language"),n("google_basket_discount","vtp_discount","discount"),n("google_conversion_items","vtp_items","items"),l.google_conversion_items=l.google_conversion_items.map(function(v){return{value:v.price,quantity:v.quantity,item_id:v.id}}));var q=function(v,w){(l.google_additional_conversion_params=l.google_additional_conversion_params||
{})[v]=w},u=function(v){return function(w,y,x,z){var C="DATA_LAYER"==v?U(x):k[y];z(C)&&q(w,C)}},p=-1==navigator.userAgent.toLowerCase().indexOf("firefox")?"//www.googleadservices.com/pagead/conversion_async.js":"https://www.google.com/pagead/conversion_async.js";k.vtp_enableNewCustomerReporting&&(n=u(k.vtp_newCustomerReportingDataSource),n("vdnc","vtp_awNewCustomer","new_customer",function(v){return void 0!=v&&""!==v}),n("vdltv","vtp_awCustomerLTV","customer_lifetime_value",function(v){return void 0!=
v&&""!==v}));!k.hasOwnProperty("vtp_enableConversionLinker")||k.vtp_enableConversionLinker?(k.vtp_conversionCookiePrefix&&(l.google_gcl_cookie_prefix=k.vtp_conversionCookiePrefix),l.google_read_gcl_cookie_opt_out=!1):l.google_read_gcl_cookie_opt_out=!0;var t=!0;t&&b.push(l);a||(a=!0,R(p,f(),e(p)))};Z.__awct=h;Z.__awct.b="awct";Z.__awct.g=!0;Z.__awct.priorityOverride=0}();
Z.a.bzi=["nonGoogleScripts"],function(){(function(a){Z.__bzi=a;Z.__bzi.b="bzi";Z.__bzi.g=!0;Z.__bzi.priorityOverride=0})(function(a){try{D._bizo_data_partner_id=a.vtp_id,D._bizo_data_partner_title=a.vtp_title,D._bizo_data_partner_domain=a.vtp_domain,D._bizo_data_partner_company=a.vtp_company,D._bizo_data_partner_location=a.vtp_location,D._bizo_data_partner_employee_range=a.vtp_employeeRange,D._bizo_data_partner_sics=a.vtp_standardIndustrialClassification,D._bizo_data_partner_email=a.vtp_email,R(Q("https://sjs",
"http://js",".bizographics.com/insight.min.js"),a.vtp_gtmOnSuccess,a.vtp_gtmOnFailure)}catch(b){G(a.vtp_gtmOnFailure)}})}();Z.a.smm=["google"],function(){(function(a){Z.__smm=a;Z.__smm.b="smm";Z.__smm.g=!0;Z.__smm.priorityOverride=0})(function(a){var b=a.vtp_input,c=Rj(a.vtp_map,"key","value")||{};return c.hasOwnProperty(b)?c[b]:a.vtp_defaultValue})}();




Z.a.paused=[],function(){(function(a){Z.__paused=a;Z.__paused.b="paused";Z.__paused.g=!0;Z.__paused.priorityOverride=0})(function(a){G(a.vtp_gtmOnFailure)})}();
Z.a.html=["customScripts"],function(){function a(d,e,f,h){return function(){try{if(0<e.length){var k=e.shift(),l=a(d,e,f,h);if("SCRIPT"==String(k.nodeName).toUpperCase()&&"text/gtmscript"==k.type){var m=F.createElement("script");m.async=!1;m.type="text/javascript";m.id=k.id;m.text=k.text||k.textContent||k.innerHTML||"";k.charset&&(m.charset=k.charset);var n=k.getAttribute("data-gtmsrc");n&&(m.src=n,ic(m,l));d.insertBefore(m,null);n||l()}else if(k.innerHTML&&0<=k.innerHTML.toLowerCase().indexOf("<script")){for(var q=
[];k.firstChild;)q.push(k.removeChild(k.firstChild));d.insertBefore(k,null);a(k,q,l,h)()}else d.insertBefore(k,null),l()}else f()}catch(u){G(h)}}}var c=function(d){if(F.body){var e=
d.vtp_gtmOnFailure,f=kj(d.vtp_html,d.vtp_gtmOnSuccess,e),h=f.Jc,k=f.B;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(F.body,sc(h),k,e)()}else Zi(function(){c(d)},
200)};Z.__html=c;Z.__html.b="html";Z.__html.g=!0;Z.__html.priorityOverride=0}();






Z.a.lcl=[],function(){function a(){var e=X("document"),f=0,h=function(k){var l=k.target;if(l&&3!==k.which&&!(k.lg||k.timeStamp&&k.timeStamp===f)){f=k.timeStamp;l=tc(l,["a","area"],100);if(!l)return k.returnValue;var m=k.defaultPrevented||!1===k.returnValue,n=bh("lcl",m?"nv.mwt":"mwt",0),q;q=m?bh("lcl","nv.ids",[]):bh("lcl","ids",[]);if(q.length){var u=Yg(l,"gtm.linkClick",q);if(b(k,l,e)&&!m&&n&&l.href){var p=String(jj(l,"rel")||""),t=!!va(p.split(" "),function(y){return"noreferrer"===y.toLowerCase()});
t&&I("GTM",36);var v=X((jj(l,"target")||"_self").substring(1)),w=!0;if(cj(u,Og(function(){var y;if(y=w&&v){var x;a:if(t&&d){var z;try{z=new MouseEvent(k.type)}catch(C){if(!e.createEvent){x=!1;break a}z=e.createEvent("MouseEvents");z.initEvent(k.type,!0,!0)}z.lg=!0;k.target.dispatchEvent(z);x=!0}else x=!1;y=!x}y&&(v.location.href=jj(l,"href"))}),n))w=!1;else return k.preventDefault&&k.preventDefault(),k.returnValue=!1}else cj(u,function(){},n||2E3);return!0}}};nc(e,"click",h,!1);nc(e,"auxclick",h,
!1)}function b(e,f,h){if(2===e.which||e.ctrlKey||e.shiftKey||e.altKey||e.metaKey)return!1;var k=jj(f,"href"),l=k.indexOf("#"),m=jj(f,"target");if(m&&"_self"!==m&&"_parent"!==m&&"_top"!==m||0===l)return!1;if(0<l){var n=bj(k),q=bj(h.location);return n!==q}return!0}function c(e){var f=void 0===e.vtp_waitForTags?!0:e.vtp_waitForTags,h=void 0===e.vtp_checkValidation?!0:e.vtp_checkValidation,k=Number(e.vtp_waitForTagsTimeout);if(!k||0>=k)k=2E3;var l=e.vtp_uniqueTriggerId||"0";if(f){var m=function(q){return Math.max(k,
q)};ah("lcl","mwt",m,0);h||ah("lcl","nv.mwt",m,0)}var n=function(q){q.push(l);return q};ah("lcl","ids",n,[]);h||ah("lcl","nv.ids",n,[]);gj("lcl")||(a(),hj("lcl"));G(e.vtp_gtmOnSuccess)}var d=!1;d=!0;Z.__lcl=c;Z.__lcl.b="lcl";Z.__lcl.g=!0;Z.__lcl.priorityOverride=0;}();


var vm={};vm.macro=function(a){if(Vg.xc.hasOwnProperty(a))return Vg.xc[a]},vm.onHtmlSuccess=Vg.be(!0),vm.onHtmlFailure=Vg.be(!1);vm.dataLayer=Ld;vm.callback=function(a){ed.hasOwnProperty(a)&&qa(ed[a])&&ed[a]();delete ed[a]};function wm(){Vc[Uc.s]=vm;Ia(fd,Z.a);zb=zb||Vg;Ab=pe}
function xm(){Qh.gtm_3pds=!0;Vc=D.google_tag_manager=D.google_tag_manager||{};if(Vc[Uc.s]){var a=Vc.zones;a&&a.unregisterChild(Uc.s)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)rb.push(c[d]);for(var e=b.tags||[],f=0;f<e.length;f++)vb.push(e[f]);for(var h=b.predicates||[],k=0;k<
h.length;k++)tb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var n=l[m],q={},u=0;u<n.length;u++)q[n[u][0]]=Array.prototype.slice.call(n[u],1);sb.push(q)}xb=Z;yb=Kj;wm();Ug();te=!1;ue=0;if("interactive"==F.readyState&&!F.createEventObject||"complete"==F.readyState)we();else{nc(F,"DOMContentLoaded",we);nc(F,"readystatechange",we);if(F.createEventObject&&F.documentElement.doScroll){var p=!0;try{p=!D.frameElement}catch(y){}p&&xe()}nc(D,"load",we)}sg=!1;"complete"===F.readyState?ug():nc(D,
"load",ug);a:{if(!Ad)break a;D.setInterval(Bd,864E5);}
bd=(new Date).getTime();
}}xm();

})()
